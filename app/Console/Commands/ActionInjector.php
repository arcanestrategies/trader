<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use App\Models\Action as Action;
use App\Http\Controllers\CoinController;
use App\Http\Controllers\StockController;

class ActionInjector extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'order:create {sign} {action} {interval} {emas} {price=coin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Injects a manual order.';
	
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$pid = $this->argument('sign');					// ie. "ETH-BTC"
		$interval = $this->argument('interval');		// ie. "60" in seconds
		$bands = $this->argument('emas');	// ie. "12-26"
		$price = $this->argument('price');
		
		if(is_numeric($price)){
			$type = 'limit';
		} else {
			if($price=='coin'){
				$product = new CoinController($pid);
			} else {
				$product = new StockController($pid);
			}
			$price = (false!==($ticker = $product->getTicker()))? $ticker->price : 1;
			$type = 'market';
		}
		$what = $this->argument('action');
		$emas = explode('-',$bands);	// ie. "12-26"
		$lower = min($emas);
		$upper = max($emas);
		$time = time();
		if(!in_array($lower,['9','12','24','26','50'])||!in_array($upper,['9','12','24','26','50'])){
			Log::debug('Invalid Emas');
			return false;
		}
		try{
			$action = new Action();
			$action->sign = $pid;
			$action->time = $time;
			$action->action = $what;
			$action->price = $action->order_price = $action->projected_price = $price;
			$action->increment = $interval;
			$action->bands = $bands;
			$action->reason = "manual";
			$action->type = $type;
			$action->save();
			return true;
		} catch(\Exception $e){
			Log::debug($e);
			return false;
		}
		return false;
    }
}
