<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail as Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\CoinController;
use App\Http\Controllers\CoinExchangeController;
use App\Notifications\ActionAlert;
use App\Models\Action as Action;

class OrderProcessor extends Command
{
	use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'coin:order {sign} {interval} {emas}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes orders for stored actions';
	
	/**
		Coinbase order statuses are:
		"pending": Order received and not yet processed
		"open": Limit order exists on the order book
		"active": Stop order exists on order book (we don't do any stop orders here)
		"done": Order no longer resting on the order book (executed)
		"rejected": Could not be placed
		"cancelled": Canceled by user
		
		CRITICAL REFACTOR:
		New version must move the order process to a separate command which runs every minute
		
		New version will allow slack or SMS two-way notifications:
			(1) First we'll send a notification in the CoinController when the action is stored, with the price and link.
			(2) User can then reply to override an order (ie. manual rejection).  We may want to create a different order status for this
			(3) User can also choose a different USD amount for this order
			(4) If the user does not reply before the next execution, it'll automatically run
	**/
	public function order($latest,$pid,$price,$quote,$transaction_amount=null,$decimals=null,$order_type=null,$notify=true,$limit_method=false){
			$exchange = new CoinExchangeController();
			if(empty($transaction_amount)){
				$transaction_amount = Config::get('services.algorithm.amount.default');
			}
			/**
				if $funds is true, this will be used, else the 1st (base currency) will be used
				We want to buy and sell $1,000 USD worth of each, so since USD is typically the quote currency, we would ordinarily use $funds and set it to $1,005 to cover the transaction fee
				However, in the case of a non-USD exchange, we need to determ
				**/
			if(empty($order_type)){
				$order_type = Config::get('services.algorithm.order_type');
			}
			if($quote=='USD'){
				if(empty($price)){
					try{
						$quote_product = new CoinController($pid);
						$quote_product = $quote_product->getTicker();
						$price = $quote_product->price;
					} catch (\Exception $e){
						Log::debug('Failure '.$e);
						return false;
					}
				}
				$fee = $transaction_amount*Config::get('services.coinbase.fee');	// This is how much it costs us to do a buy+sale transaction
				// Since we are working with a fixed dollar pool, we need to deduct the fee amount from re-buys, otherwise our pool will keep getting smaller.
				// REFACTOR: The fee is actually stored as double the fee in order to cover buy the buy and sell side which isn't really the right way to do this.
				//				When we change that, we need to change all instances of fee to fee*2.
				if($latest->action=='sell'){
					$fee = 0;
				}
				if(isset($latest->size)&&!empty($latest->size)&&($latest->size)>0){
					$product = new CoinController($pid);
					if(isset($product->base_min_size) && $latest->size < $product->base_min_size){
						$size = $product->base_min_size;
					} else {
						$size = ($latest->size) - ($latest->size * Config::get('services.coinbase.fee'));
					}
				} else {
					$size = (($transaction_amount-$fee)/$price);	// This is how much of a coin to buy (ie. 0.25 ETH).
				}
			} else {
				// REFACTOR CRITICAL: This hasn't been tested yet.
				if(empty($price)){
					try{
						$quote_product = new CoinController($quote.'-USD');
						$quote_product = $quote_product->getTicker();
						$quote_price = $quote_product->price;
					} catch (\Exception $e){
						Log::debug('Failure '.$e);
						return false;
					}
				}
				$fee = $transaction_amount*Config::get('services.coinbase.fee');
				if($latest->action=='sell'){
					$fee = 0;
				}
				// Now that we have the quote price (ie. 50,000 USD for BTC): $quote_price
				// We already have our order price (ie. 0.04 ETH-BTC): $price
				// To determine the size, we take the amount (ie. 100 USD) and divide the quote price by the USD amount (ie. 100/50,000=0.002 BTC
				// That basically gives us a new "algorithm amount" which we have to re-divide by the order price
				if(isset($latest->size)&&!empty($latest->size)&&$latest->size>0.0){
					$product = new CoinController($pid);
					if(isset($product->base_min_size) && $latest->size < $product->base_min_size){
						$size = $product->base_min_size;
					} else {
						$size = ($latest->size) - ($latest->size * Config::get('services.coinbase.fee'));
					}
				} else {
					$size = (($transaction_amount-$fee)/$quote_price);
					// So, we need to order 0.002BTC worth of ETH at 0.04 which means we divide 0.04
					$size = ($size/$price);
				}
			}
			if(in_array($latest->action,['sell','buy'])&&!empty($price)){
				$order = false;
				if(Config::get('app.env')=='live'||Config::get('app.env')=='production'){
					$order = $exchange->placeOrder($latest->action,$order_type,$price,$size,$pid,$limit_method);
					Log::debug('Order Attempted: '.$pid.' '.time().' '.json_encode($order));
				}
				$event = 'Executed';
				$minutes = $latest->increment/60;
				$host = Config::get('app.url');
				$emas = str_replace('-',',',$latest->bands);
				$url = $host.'?emas='.$emas.'&increment='.$latest->increment.'&product='.$pid;
				if(!isset($product)){
					$product = new CoinController($pid);
				}
				$notice_price = (false!==($ticker = $product->getTicker()))? $ticker->price : $price;
				$message = $event.' '.ucfirst($order_type).' '.ucfirst($latest->action).' '.$pid.' @ '.$notice_price.' due to '.$latest->reason.' on '.$minutes.'minute '.$emas.': '.$url;
				if(false!==$order){
					$latest->order_id = $order->id;
					$latest->status = $order->status;
					$latest->price = $price;
					$latest->order_price = $price;
					$latest->type = $order_type;
					$latest->size = $size;
					$latest->filled = 0;
					$latest->save();
					if($notify==true&&(Config::get('app.env')=='live'||Config::get('app.env')=='production')){
						Notification::route('slack', Config::get('services.slack.webhook_coin'))->notify(new ActionAlert($message,'#crypto'));
						if(($latest->increment) >= 3600){
							Notification::route('slack', Config::get('services.slack.webhook_btc'))->notify(new ActionAlert($message,'#btc_60m'));
							//Notification::route('nexmo', Config::get('services.nexmo.sms_to'))->notify(new ActionAlert($message));
						}
					}
					return true;
				}
			}
			return false;
	}
	
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$pid = $this->argument('sign');					// ie. "ETH-BTC"
		$interval = intval($this->argument('interval'));		// ie. "60" in seconds
		$emas = explode('-',$this->argument('emas'));	// ie. "12-26"
		$lower = min($emas);
		$upper = max($emas);
		$product = new CoinController($pid);
		$product->save_results = true;
		$now = time();
		$price = (false!==($ticker = $product->getTicker()))? $ticker->price : null;
		$min_size = isset($product->base_min_size)? $product->base_min_size : 0.0;
		$decimals = strlen(strrchr($product->quote_increment, '.')) -1;
		$variance = Config::get('services.algorithm.limit_variance');
		$transaction_amount = (null !== Config::get('services.algorithm.amount.'.$interval)) ? Config::get('services.algorithm.amount.'.$interval) : Config::get('services.algorithm.amount.default');
		if(!empty($product->product)){
			// Action data should already be processed in the other cron job, so now we just fill orders
			try{
				$quote = explode('-',$pid)[1];				// Quote currency is always the 2nd param;
				// It is possible to get multiple actions within an increment, although very unlikely, so chances are we want to stick with just 1 order
				$latest = Action::orderBy('time','DESC')
					->where(['increment'=>$interval,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
					->where('time','<=',$now)
					->where(function ($q){
						$q->where('status','!=','wait')
						->orWhereNull('status');
					})->first();
				if(!empty($latest)){
					if(in_array($latest->status,["done","cancelled","rejected"])){
						return true;
					}
					if(empty($price)){
						$price = $latest->price;
					}
					if($latest->action=='sell'){
						$new_price = (!isset($latest->limit_method)||($latest->limit_method!=='post_only'))? ($price-($price*$variance)) : $price;
						$new_price = max($new_price,$latest->price);
						// The thresher is a price that indicates our flag was wrong. If right, the sell price should be lower than this one, otherwise it is a reversal
						$thresher = ($latest->price)+($latest->price*($variance*3.5));
					} else {
						$new_price = (isset($latest->limit_method)&&($latest->limit_method!=='post_only'))? ($price+($price*$variance)) : $price;
						$new_price = min($new_price,$latest->price);
						// The thresher is a price that indicates our flag was wrong. If right, the buy price should be higher than this one. 
						$thresher = ($latest->price)-($latest->price*($variance*3.5));
					}
					$exchange = new CoinExchangeController();
					// If the latest action had an order open, check on the status and update it.
					if(!empty($latest->status)&&in_array($latest->status,['open','pending'])&&!empty($latest->order_id)){
						try {
							$order = $exchange->getOrder($latest->order_id);
							Log::debug('Order: '.$pid.' '.time().' '.json_encode($order));
							$reorder = false;
							$filled = false;
							// CRITICAL REFACTOR: Need a way to reconcile past orders that aren't this one and update the fill amount.
							if(!empty($order)){
								$latest->status = $order->status;
								if(in_array($order->status,['open','pending'])){
									if($order->filled_size >= ($min_size*10)){
										$filled = true;
									}
									// If order still open and the price has moved too much for it to execute, cancel the order and change the status
									if(!empty($price)&&(($latest->action=='buy'&&($latest->price < ($price - ($price*$variance) )))||($latest->action=='sell'&&($latest->price > ($price + ($price*$variance) ))))){
										// Cancel the order
										$cancel = $exchange->cancelOrder($latest->order_id);
										if(!empty($cancel) && $filled===false){
											$event = 'Cancelled';
											$minutes = $latest->increment/60;
											$emas = str_replace('-',',',$latest->bands);
											$message = $event.' '.ucfirst($latest->action).' '.$pid.' on the '.$minutes.'minute charts with '.$emas.' EMAs';
											if($interval>=900&&(Config::get('app.env')=='live'||Config::get('app.env')=='production')){
												//Notification::route('nexmo', Config::get('services.nexmo.sms_to'))->notify(new ActionAlert($message));
												Notification::route('slack', Config::get('services.slack.webhook_coin'))->notify(new ActionAlert($message,'#crypto'));
											}
											// If the price is still actionable, re-run it at the new price level, else mark it as a wait action.
											// It is actionable if the new price is greater than the ticker price AND the ticker price is not lower than last price minus variance
											// Anything using the short intervals (1m-5m) will attempt another limit order, else market order
											if(($latest->action=='buy'&&($price > $thresher))||($latest->action=='sell'&&($price < $thresher))){
												if(($latest->action=='buy'&&($new_price >= $price))||($latest->action=='sell'&&($new_price <= $price))){
													$type = 'limit';
												} else {
													$type = 'market';
												}
												$notify = true;
												if(isset($latest->limit_method)&&($latest->limit_method==='post_only')&&$type==='market'){
													// Do nothing.  Post only methods are not actions we want to chase, so if it cannot be filled as a limit order, do not reorder it, just set it as a wait
													$latest->status = 'wait';
													$latest->limit_method = 'post-only-reorder-non-actionable';
												} else {
													Log::debug($pid.' Reordering the last action '.time());
													$reorder = self::order($latest,$pid,$new_price,$quote,$transaction_amount,$decimals,$type,$notify);
													if($reorder===false){
														$latest->status = 'cancelled';
														$latest->limit_method = 'reorder-function-failed';
													}
												}
											} else {
												$latest->status = 'cancelled';
												$latest->limit_method = 'reorder-thresher-exceeded';
											}
										}
									}
								}
								if(in_array($order->status,['open','done'])){
									try {
										if($order->filled_size > 0.0){						
											$latest->filled = $order->filled_size;
											$latest->size = $order->size;
											$latest->price = ($order->executed_value/$latest->filled);
											$latest->status = 'done'; // overrides cancelled status if an order is partially filled and then cancelled
											// The order processed at least partially
											/*$fills = $exchange->getFills($latest->order_id);
											$filled = 0;
											$cost = 0;
											foreach($fills as $fill){
												if(isset($fill->size)&&!empty($fill->size)&&$fill->settled===true){
													$filled = $filled+($fill->size);
													$cost = $cost+(($fill->size) * ($fill->price));
												}
											}
											if(!empty($cost)&&!empty($filled)){
												$price = $cost/$filled;
												$latest->price = $price;
												$latest->filled = $filled;
											}*/
										}
										// REFACTOR: Now store the margin from here to the last order. If a sale + if higher. If a buy + if lower.
									} catch (\Exception $e){
										Log::debug('Fills API Failed');
										Log::debug($e);
									}
								}
								$latest->save();
								return true;
							}
						} catch (\Exception $e){
							Log::debug('Order API Failed');
						}
					}
					// If this is the latest action relative to current timeframe and nothing's been done about it, fill it.	
					// Or if this latest action and it is actionable related to price and nothing's been done about it, fill it.
					if((empty($latest->status)||!in_array($latest->status,['done','wait','open','pending','rejected','cancelled']))){
						$islatest = $isactionable = false;
						if($now-$interval < $latest->time){
							$islatest = true;
						}
						
						$caution = true;
						if(($latest->action=='buy'&&($price > $thresher))||($latest->action=='sell'&&($price < $thresher))){
							$caution = false;
							if(($latest->action=='buy'&&($new_price >= $price))||($latest->action=='sell'&&($new_price <= $price))){
								$isactionable = true;
							}
						}
						$history = $product->getHistory($latest->time,0,$interval);
						$key = array_search($latest->time, array_column($history,$product->time_key));
						$candle = $history[$key];
						$slope = ($candle[$product->close_key] > $candle[$product->open_key])? 'buy' : 'sell';
						if($latest->action !== $slope){
							$caution = true;
						}
						if($caution === false){
							if($isactionable === true || $islatest === true){
								Log::debug($pid.' Ordering '.time());
								$notify = ($interval >= 900)? true : false;
								if(isset($latest->type) && !empty($latest->type)){
									$order_type = $latest->type;
								} else {
									$order_type = 'limit';
								}
								$limit_method = false;
								if(isset($latest->limit_method) && $latest->limit_method === 'post_only'){
									$limit_method = true;
								}
								if($isactionable === false && $limit_method === false){
									$order_type = 'market';
								}
								if($interval < 3600 || Config::get('services.algorithm.minute_triggers')!==true){
									$rrr = self::order($latest,$pid,$new_price,$quote,$transaction_amount,$decimals,$order_type,$notify,$limit_method);
									// If this is a 5m candle, check to see if there's an hourly candle with the same action and if so, take it.
									// There's always the possibility that there's no buy action to follow
									if($interval == 300 && Config::get('services.algorithm.minute_triggers')===true){
										$start_last_hour = (($latest->time) - 3300);
										$latest_hourly_action = Action::orderBy('time','DESC')
																->where(['increment'=>3600,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
																->where('time','>=',$start_last_hour)
																->where(function ($q){
																	$q->where('status','wait')
																	->orWhereNull('status');
																})->first();
										if(!empty($latest_hourly_action)){
											if($latest_hourly_action->action == $latest->action){
												$rrl = self::order($latest_hourly_action,$pid,$new_price,$quote,$transaction_amount,$decimals,'market',true,'any');
											}
										}
									}
									return $rrr;
								} else {
									$latest->limit_method = 'pending-short-candle';
									$latest->save();
								}
							} else {
								Log::debug('Order Action Late and Non-Actionable on '.$pid.' at '.$now.' for interval '.$interval.', creating '.($now-$interval).', where time was '.$latest->time);
								$latest->limit_method = 'late-and-non-actionable';
								$latest->status = 'cancelled';
								$latest->save();
								return true;
							}
						} else {
							$latest->limit_method = 'thresher-exceeded';
							$latest->status = 'cancelled';
							$latest->save();
							return true;
						}
					}
				}
				return false;
			} catch(\Exception $e){
				Log::debug('Failure '.$e);
				return false;
			}
		}
		return false;
    }
}
