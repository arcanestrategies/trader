<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use App\Http\Controllers\CoinController;
use App\Models\Macd as Macd;
use App\Models\Action as Action;
use App\Models\Ema as Ema;

class RealTimeCoinTracker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'coin:track {sign} {interval} {emas}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the acitons on a recurring basis for a given stock and interval.';
	
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$pid = $this->argument('sign');					// ie. "ETH-BTC"
		$interval = $this->argument('interval');		// ie. "60" in seconds
		//Log::debug('Starting '.$pid.' '.$interval.' '.time());
		$emas = explode('-',$this->argument('emas'));	// ie. "12-26"
		$lower = min($emas);
		$upper = max($emas);
		$product = new CoinController($pid);
		$product->save_results = true;
		if(!in_array($lower,['9','12','24','26','50'])||!in_array($upper,['9','12','24','26','50'])){
			return false;
		}
		if(!empty($product->product)){
			// Process everything...
			try{
				Log::debug('Starting '.$pid.' '.$interval);
				$product->getMACD($emas, null, $interval);		// This runs SMA, EMA, and Bollinger Bands
				$levels = $product->getHistoricLevels($interval, $pid);	// This forms all of the support and resistance levels (must run after macd)
				$actions = $product->getActions($emas, null, $interval);	// This creates all the actions based on macd and levels.
				Log::debug('Finished '.$pid.' '.$interval);
			} catch(\Exception $e){
				Log::debug($pid.' Histories Failed At '.time());
				Log::debug($e);
				return false;
			}
		}
		return false;
    }
}
