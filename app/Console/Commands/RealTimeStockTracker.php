<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail as Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\StockController;
use App\Http\Controllers\StockExchangeController;
use App\Notifications\ActionAlert;
use App\Models\Macd as Macd;
use App\Models\Action as Action;
use App\Models\Ema as Ema;

class RealTimeStockTracker extends Command
{
	use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'stock:macd {sign} {interval} {emas}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the MACD on a recurring basis for a given stock and interval.';
	
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$pid = $this->argument('sign');				// ie. "FB"
		$interval = $this->argument('interval');		// ie. "60" in seconds
		$emas = explode('-',$this->argument('emas'));	// ie. "12-26"
		$range = null;
		$product = new StockController($pid);
		if(!empty($product->product)){
			$decimals = strlen(strrchr($product->quote_increment, '.')) -1;
			try{
				$quote = 'USD'; // All stocks are USD quoted
				$product->getMACD($emas, $range, $interval);
				$latest = Macd::orderBy('time','DESC')->where('bands',$this->argument('emas'))->where('sign',$pid)->where('increment',$interval)->take(1)->first();
				if(!empty($latest)){
					$exchange = new StockExchangeController();
					// If the latest action had an order, check on the status and update it.
					if($latest->status=='open'&&!empty($latest->order_id)){
						$order = $exchange->getOrder($latest->order_id);
						$latest->status = $order->status;
						$latest->save();
						// If this is the latest action and nothing's been done about it, fill it.						
					} else if(time()-$interval < $latest->time && (empty($latest->status)||!in_array($latest->status,['done','open']))){
						/**
							if $funds is true, this will be used, else the 1st (base currency) will be used
							We want to buy and sell $1,000 USD worth of each, so since USD is typically the quote currency, we would ordinarily use $funds and set it to $1,005 to cover the transaction fee
							However, in the case of a non-USD exchange, we need to determ
						**/
						if($quote=='USD'){
							$size = Config::get('services.algorithm.amount');
						}
						$price = (false!==($ticker = $product->getTicker()))? $ticker->price : null;
						// REFACTOR: For now we're doing market orders because we need to write extra logic to handle limit orders so they don't sit around unfilled
						$order = $exchange->placeOrder($latest->action,'market',$price,$size,$pid,true);
						if(false!==$order){
							$latest->order_id = $order->id;
							$latest->status = $order->status;
							$latest->save();
							$message = 'Order Executed '.ucfirst($latest->action).' '.$pid.' at '.$price;
							Notification::route('nexmo', Config::get('services.nexmo.sms_to'))->notify(new ActionAlert($message));
							return true;
						} else {
							Log::debug($order);
						}
					}
				}
				return false;
			} catch(\Exception $e){
				Log::debug('Failure '.$e);
				return false;
			}
		}
		return false;
    }
}