<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\RealTimeCoinTracker;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // NOTE: 9-26 performs slightly (razor thin slightly) better, so we will move everything to that.
		
		$schedule->command('coin:track BTC-USD 900 9-26')->everyTwoMinutes()->withoutOverlapping(3);
		$schedule->command('coin:order BTC-USD 900 9-26')->everyMinute()->withoutOverlapping(2);

		$schedule->command('coin:track BTC-USD 300 9-26')->everyTwoMinutes()->withoutOverlapping(3);
		$schedule->command('coin:order BTC-USD 300 9-26')->everyMinute()->withoutOverlapping(2);

		$schedule->command('coin:track BTC-USD 3600 9-26')->everyFiveMinutes()->withoutOverlapping(3);
		$schedule->command('coin:order BTC-USD 3600 9-26')->everyMinute()->withoutOverlapping(2);

		//$schedule->command('coin:track BTC-USD 900 12-26')->everyTwoMinutes()->withoutOverlapping(3);
		//$schedule->command('coin:order BTC-USD 900 12-26')->everyMinute()->withoutOverlapping(2);

		//$schedule->command('coin:track BTC-USD 300 12-26')->everyTwoMinutes()->withoutOverlapping(3);
		//$schedule->command('coin:order BTC-USD 300 12-26')->everyMinute()->withoutOverlapping(2);

		//$schedule->command('coin:track BTC-USD 3600 12-26')->everyTwoMinutes()->withoutOverlapping(3);
		//$schedule->command('coin:order BTC-USD 3600 12-26')->everyMinute()->withoutOverlapping(2);
		
		//$schedule->command('coin:track DASH-USD 900 12-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:track DASH-USD 900 12-26')->everyTwoMinutes()->withoutOverlapping(3);
		$schedule->command('coin:order DASH-USD 900 12-26')->everyMinute()->withoutOverlapping(2);
		
		//$schedule->command('coin:track ETC-USD 900 12-26')->everyTwoMinutes()->withoutOverlapping(3);
		//$schedule->command('coin:order ETC-USD 900 12-26')->everyMinute()->withoutOverlapping(2);

		$schedule->command('coin:track ETC-USD 3600 9-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order ETC-USD 3600 9-26')->everyMinute()->withoutOverlapping(2);

		$schedule->command('coin:track ETH-USD 3600 12-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order ETH-USD 3600 12-26')->everyMinute()->withoutOverlapping(2);

		$schedule->command('coin:track ETH-USD 300 12-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order ETH-USD 300 12-26')->everyMinute()->withoutOverlapping(2);
		
		$schedule->command('coin:track LTC-USD 900 12-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order LTC-USD 900 12-26')->everyMinute()->withoutOverlapping(2);
		
		$schedule->command('coin:track LTC-USD 3600 9-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order LTC-USD 3600 9-26')->everyMinute()->withoutOverlapping(2);
				
		$schedule->command('coin:track BCH-USD 300 12-26')->cron('1-59/2 * * * *')->withoutOverlapping(3);
		$schedule->command('coin:order BCH-USD 300 12-26')->everyMinute()->withoutOverlapping(2);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
