<?php

namespace App\Http\Controllers;
use App\Http\Controllers\CoinApiController;

class AccountController extends Controller {
	public function __construct(){
		// DO NOTHING
	}

	/**
		Pass "*" to get all
	**/
	public function getAccount($id){
        $api = new CoinApiController();
		if($id=="*"){
			return $api->run('accounts');
		}
		return $api->run('accounts/'.$id);
	}

	public function getCoinbaseAccounts(){
        $api = new CoinApiController();
		return $api->run('coinbase-accounts');
	}

	public function getPaymentMethods(){
        $api = new CoinApiController();
		return $api->run('payment-methods');
	}

	public function getAccountHistory($id){
        $api = new CoinApiController();
		return $api->run('accounts/'.$id.'/ledger');
	}

}

?>
