<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Config;

// REFACTOR this is specifically for coinbase, so this should be renamed everywhere

class CoinApiController extends Controller {
	/**
		200 Success
		400 Bad Request
		401 Unauthorized
		403 Forbidden
		429 Too Many Requests // 3 Requests Per second public, 5 private, 500 total orders per account may remain open
		404 Not Found
		500 Server Error
	**/
	public $limit = 100;
	public $endpoint = null;
	public static $LAST_STATUS = 0;

	public function __construct(){
		// DO NOTHING
	}

    public function signature($request_path='', $body='', $timestamp=false, $method='GET') {
        $body = is_array($body) ? json_encode($body) : $body;
        $timestamp = $timestamp ? $timestamp : time();

        $what = $timestamp.$method.$request_path.$body;

        return base64_encode(hash_hmac("sha256", $what, base64_decode(Config::get('services.coinbase.secret')), true));
    }

	public function run($path,$method='GET',$data=null){
			$session = curl_init();
			$time = time();
			$signature = self::signature('/'.$path,$data,$time,strtoupper($method));
			$url = Config::get('services.coinbase.endpoint').'/'.$path;
			$headers = [
							'Content-Type: application/json',
							'User-Agent: telnet',
							'CB-ACCESS-KEY: '.Config::get('services.coinbase.key'),
							'CB-ACCESS-SIGN: '.$signature,
							'CB-ACCESS-TIMESTAMP: '.$time,
							'CB-ACCESS-PASSPHRASE: '.Config::get('services.coinbase.passphrase'),
						];

			switch ($method)
			{
				case "PATCH":
				case "PUT":
				case "POST":
					curl_setopt($session, CURLOPT_POST, 1);
					if (!empty($data)){
						$data = json_encode($data);
						curl_setopt($session, CURLOPT_POSTFIELDS, $data);
					}
					break;
				case "DELETE":
					curl_setopt($session, CURLOPT_CUSTOMREQUEST, "DELETE");
					break;
				case "GET":
				default:
					if (!empty($data)){
						$get = http_build_query($data);
						$url .= '?'.$get;
					}
					curl_setopt($session, CURLOPT_HTTPGET, 1);
					break;
			}

			curl_setopt($session, CURLOPT_URL, $url);
			curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($session, CURLOPT_SSL_VERIFYPEER,false);
			$response = curl_exec($session);

			self::$LAST_STATUS = curl_getinfo($session, CURLINFO_HTTP_CODE);
			curl_close($session);
			$response = json_decode($response);
			if(self::$LAST_STATUS!==200){
				Log::debug('status:'.self::$LAST_STATUS);
				Log::debug('url:'.$url);
				Log::debug('data:'.json_encode($data));
				Log::debug('response:'.json_encode($response));
				return false;
			}
			return $response;
	}

	public function paginate($id,$direction="after",$endpoint=null){
		if($endpoint===null&&$this->endpoint!==null){
			$endpoint = $this->endpoint;
		}
		if($endpoint===null){
			return false;
		}
		$data = [$direction=>$id];
		return self::run($endpoint,'GET',$data);
	}
}

?>
