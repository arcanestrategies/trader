<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use App\Http\Controllers\CoinApiController;
use App\Http\Controllers\TradeInterface;
use App\Models\Macd as Macd;
use App\Models\Action as Action;
use App\Models\Ema as Ema;
use App\Models\Sma as Sma;
use App\Models\Product as Product;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log as Log;

class CoinController extends StatisticsController implements TradeInterface  {

	public $base_currency;
	public $quote_currency;
	public $base_min_size;
	public $base_max_size;
	public $quote_increment;
	public $base_increment;
	public $min_market_funds;
	public $product = '*';
	public $agg_limit = 300;
	public $smoothing = 2;
	public $time_key = 0;
	public $low_key = 1;
	public $high_key = 2;
	public $open_key = 3;
	public $close_key = 4;
	public $fee;
	public $signals;
	public $last_slope = null;
	public $new_slope = null;
	public $counter = 1;
	public $history = [];
	public $levels = [];
	public $repeat_levels = [];
	public $bollinger_upper = [];
	public $bollinger_lower = [];
	public $band_margin = [];
	public $deviation_band = [];
	public $breaks = [];
	public $action = [];
	public $save_results = false;
	public $gradient = null;
	public $limit_variance;

	public function __construct($id=null){
		$this->fee = Config::get('services.coinbase.fee');
		$this->limit_variance = Config::get('services.algorithm.limit_variance');
		$this->fee = $this->fee + ($this->limit_variance * 2);
		if(null!==$id){
			$this->product = $id;
			$obj = self::getProduct($id);
			if(!empty($obj)){
				foreach($obj as $p => $v){
					$this->$p = $v;
				}
			} else {
				$this->product = null;
			}
		}
	}

	public static function getPrice($id){
		$product = new CoinController($id);
		$stats = $product->getTicker();
		if(!empty($stats)){
			return $stats->price;
		}
		return false;
	}

	public function getAPITime(){
        $api = new CoinApiController();
		return $api->run('time');
	}

	/**
		Pass "*" to get all
	**/
	public function getCurrency($id){
        $api = new CoinApiController();
		if(!empty($id)){
			if($id=="*"){
				return $api->run('currencies');
			} else {
				return $api->run('currencies/'.$id);
			}
		}
		return $api->run('currencies/'.$this->product);
	}

	/**
		Pass "*" to get all
	**/
	public function getProduct($id=null){
        $api = new CoinApiController();
		if(empty($id)){
			$id = $this->product;
		}
		if($id=="*"){
			$getproducts = Product::where('type','coin')->get();
			if(empty($getproducts)){
				// Get all products from API and store them to the database
				$getproducts = $api->run('products');
				foreach($getproducts as $product){
					// REFACTOR: This should be a recursive function since we do the same for 1 id.
					$p = Product::where('sign',$product->id)->where('type','coin')->first();
					if(empty($p)){
						$p = new Product();
					}
					foreach($product as $key=>$value){
						if($key=='id'){
							$key = 'sign';
						}
						$p->$key = $value;
					}
					$p->type = 'coin';
					$p->save();
				}
			}
			return Product::where('type','coin')->get();
		} else {
			$p = Product::where('sign',$id)->where('type','coin')->first();
			$getprod = $api->run('products/'.$id);
			if(!empty($getprod)){
				if(empty($p)){
					$p = new Product();
				}
				foreach($getprod as $key=>$value){
					if($key=='id'){
						$key = 'sign';
					}
					$p->$key = $value;
				}
				$p->type = 'coin';
				$p->save();
			}
			return $p;
		}
	}

	/**
		Real-time Data
	**/
	public function getOrderBook($level=1){
        $api = new CoinApiController();
		// 1 : Only big and ask
		// 2 : Top 50 bids and asks // shows sum of size and number orders at a given price.  Don't divide size and num.
		// 3 : Full order book (not a summary) // Only intended for use with websocket.  Abuse will result in blockage.
		return $api->run('products/'.$this->product.'/book','GET',['level'=>$level]);
	}

	/**
		This provides real-time data but they want us to use the websocket instead... this is "discouraged"
	**/
	public function getTicker($counter=0){
        $api = new CoinApiController();
		$ticker = $api->run('products/'.$this->product.'/ticker');
		if(empty($ticker)&&$counter<3){
			return self::getTicker($counter+1);
		}
		return $ticker;
	}

	/**
		Real-time Data
	**/
	public function getTrades(){
        $api = new CoinApiController();

		return $api->run('products/'.$this->product.'/trades');
	}

	/**
		24 hour trailing stats (open, high, low, volume, last, 30 day volume).
	**/
	public function getStats(){
        $api = new CoinApiController();
		return $api->run('products/'.$this->product.'/stats');
	}
	
	/**
		Proceeds from the furthest back point moving forward to determine patterns.
		(1) Set a counter for 1, 2 slope vars (last_slope, new_slope) for null, and an empty array (to become associateive), loop over history.
		(2) If current closing price is higher than previous one plus the degree of variability, set the current slope as bullish.  If lower than previous price bearish.
		(3) Proceed forward and add 1 to the counter
		(4) If the slope changes direction and last_slope was not null:
		(4.a) If the counter is greater than counter threshold (ENV 3?), store the opening price as level 1.  Store the last slope alongside it: (ie. ['time'=>['slope'=>Z,'level1'=>Y,'level2'=>X]]
			(4.a.1) if the trend is now bullish, store the previous time's low as level 2
			(4.a.2) if the trend is now bearish, store the previous time's high as level 2.
		(4.b) If the current slope is the same as the last level's slope
			(4.b.1) if the current slope is bullish, overwrite the last slope level 2 with the current high.
			(4.b.2) if the current slope is bearish, overwrite the last slope level 2 with the current low.
		(4.c) Set the counter to 1;
	**/
	public function getHistoricLevels($increment,$sign){
		$history = self::getHistory(0,0,$increment);
		if(empty($history)){
			return $this->levels;
		}
		for($i=0; $i<count($history); $i++){
			if($i==0||empty($history)||count($history)<5){
				continue;
			}
			self::getCurrentLevel($i,$history,$increment);
		}
		return $this->levels;
		//return $this->repeat_levels;
	}
	
	/**
		Checks whether a given timestamp is NOT a level but still broke the pattern:
		(1) If it is an all time high or low, it has busted through resistance or support, so we chase this. WARNING: Chasing is generally a bad idea, so this is a rarity.
		(2) If it breaks through the bollinger band and is not a level, it is a continued behavior which disagrees with the current pattern
		That indicates a runaway (breaking up or breaking down)
	**/
	
	public function isBreakaway($time,$price,$increment,$slope){
		$all_time = self::isAllChart($time,$price,$increment,$slope);
		if(false==self::isLevel($time) || false!=self::isLevel($time,$slope)){
			if($slope=='bull' && ((isset($this->bollinger_upper[$time]) && $price > $this->bollinger_upper[$time]) || false!==$all_time)){
				return true;
			}
			if($slope=='bear' && ((isset($this->bollinger_lower[$time]) && $price < $this->bollinger_lower[$time]) || false!==$all_time)){
				return true;
			}
		}
		return false;
	}
	
	/**
		Checks whether a given price is all-time high or low for a given chart (not of all time)
	**/	
	public function isAllChart($end,$price,$increment,$slope='bull'){
		// Since we are checking for all time, we can exclude now (ie. $end-$increment) or we can just check if now equals highest or lowest.
		$history = self::getHistory(0,($end-$increment),$increment);
		if(empty($history)||empty(array_column($history,2))){
			return false;
		}
		if($slope=='bull'){
			$highs = array_column($history,$this->high_key);	// REFACTOR: Coinbase API uses index 2 for high.  This differs in Alpaca
			$ath = max($highs);
			if($price>=$ath){
				return true;
			}
		}
		if($slope=='bear'){
			$lows = array_column($history,$this->low_key);	// REFACTOR: Coinbase API uses index 1 for low.  This differs in Alpaca
			$atl = min($lows);
			if($price<=$atl){
				return true;
			}
		}
		return false;
	}
	
	/**
		Checks whether a given timestamp has already been stored as a level.
		Traverses levels in reverse
		Accepts an optional slope argument to verify a level created by a reversal, not by an increased position.
	**/	
	
	public function isALevel($time,$new_slope=false){
		$l = array_search($time, array_combine(array_keys($this->levels), array_column($this->levels,'time')));
		if($l!==false){
			$l = $this->levels[$l];
			if($new_slope!==false){
				if($l['slope']!==$new_slope){
					return $l;
				}
			} else {
				return $l;
			}
		}
		return false;
	}
	
	public function isLevel($time,$new_slope=false){
		if(empty($this->levels)){
			return false;
		}
		$levels = array_reverse($this->levels,true);
		foreach($levels as $l){
			if($l['time']==$time){
				if($new_slope!=false){
					if($l['slope']!=$new_slope){
						return $l;
					}
				//} else if($l['type'] != 'continued') {
				} else {
					return $l;
				}
			}
		}
		return false;
	}
	
	/**
		REFACTOR: Should levels use a unique key l ike the $sign-$increment so we're not repeating levels with a different sign or increment?
	**/	
	public function enteredLevel($price,$time,$start=null,$slope=null){
		$result = false;
		if(!empty($this->levels)){
			foreach($this->levels as $key=>$past){
				// Start should only be used to ignore past levels (which we shouldn't do unless we're analyzing a particular segment like a head and shoulders pattern)
				if(empty($past)||(!empty($start)&&$past['time']<$start)){
					continue;
				}
				if(!empty($slope)&&$slope!==$past['slope']){
					continue;
				}
				// This is a future level (or is the current one), so we stop here.
				if($past['time']>=$time){
					break;
				}
				if($past['slope']=='bull'){	// In theory, this current point would either be a bull with a breakthrough or a bear at a level
					// Levels don't often hit exactly but can be within a small degree of variance, so we are using the average instead of the close price
					if($price <= $past['level2'] && $price >= $past['level1']){
						$result = $past;
					}
				} else if($past['slope']=='bear'){
					// Levels don't often hit exactly but can be within a small degree of variance, so we are going to shift the close price up to the average
					if($price >= $past['level2'] && $price <= $past['level1']){
						$result = $past;
					}
				}
			}
		}
		return $result;
	}
	
	/**
		REFACTOR: Currently we get all historic levels by looping over this method and then the "isLevel" function checks to see if there's a stored level already at a given timestamp and slope
		This seems a bit redundant but the "getCurrentLevel" function uses an incremental iterator as opposed to a timestamp (among other things)
	**/
	public function getCurrentLevel($i=1,$history,$increment=null,$start=null){
		if(empty($history)){
			return false;
		}
		$new = $history[$i];
		$last = $history[$i-1];
		$time = 0;
		$low = 1;
		$high = 2;
		$open = 3;
		$close = 4;
		$consecutive = Config::get('services.algorithm.level_consecutive');
		$decimals = Config::get('services.algorithm.max_decimals');
		$index = number_format($last[$close],$decimals);
		$average = ($new[$high]+$new[$low])/2;
		if($new[$close] >= $last[$close]){
			$this->new_slope = 'bull';
		} else if($new[$close] < $last[$close]){
			$this->new_slope = 'bear';
		}
		$last_level_key = array_key_last($this->levels);
		if(!empty($this->last_slope)&&$this->new_slope!==$this->last_slope){
			$crossed = 'reversal';
			// Anything that formed a consistent pattern OR exceeds or meets the bollinger bands should be considered a level
			if($this->last_slope=='bull' && isset($this->bollinger_upper[$last[$time]]) && $last[$high] >= $this->bollinger_upper[$last[$time]]){
				$crossed = 'crossed';
			}
			if($this->last_slope=='bear' && isset($this->bollinger_lower[$last[$time]]) && $last[$low] <= $this->bollinger_lower[$last[$time]]){
				$crossed = 'crossed';
			}
			// REFACTOR: This doesn't account for spikes well... For example a Bull with a spike followed by a small bull... The level will be set by the small bull instead of the previous bull's high.
			if($this->counter >= $consecutive || $crossed!=='reversal'){
				// Here we are re-setting a new level if it overwrites a previous level on the basis of consecutive behavior
				// CRITICAL REFACTOR: Since this is occurring in real time, it's possible that a candle could be falsely attributed as a level... we may want to consider that it's not really a level until the pattern changes... 
				// NOTE: We only unset levels to overwrite them on the same timestamp.  It is critical that we do not overwrite past timestamps.
				if(!empty($last_level_key)&&$this->levels[$last_level_key]['time']==$last[$time]){
					unset($this->levels[$last_level_key]);
				}
				$this->levels[$index] = [
					'time'=>$new[$time],
					'close'=>$last[$close],
					'slope'=>$this->last_slope,	// We store the last slope here because it indicates the end the previous pattern. This is a bit weird but it makes sense. Level 1 is tighter, Level 2 is further
					'type'=>$crossed
				];
				// NOTE: Problem here is that the last slope is based off the last level, so  could be the same slope type
				// (ie. http://prntscr.com/109p9kb
				if($this->last_slope=='bear'){
					$this->levels[$index]['level1'] = max($average,$last[$low]);
					$this->levels[$index]['level2'] = min($last[$low],$new[$low]);
				} else if($this->last_slope=='bull'){
					$this->levels[$index]['level1'] = min($average,$last[$high]);
					$this->levels[$index]['level2'] = max($last[$high],$new[$high]);
				}
			}
			$this->counter = 1;
		} else {
			$this->counter++;
		}
		$last_level_key = array_key_last($this->levels);
		if(!empty($last_level_key)){
			$last_level = $this->levels[$last_level_key];
			// If the current slope is the same as the last level, it has reached newer highs or lows without reaching a different level and must therefore overwrite the last level.
			// REFACTOR: Since we're only overwriting the last confirmed level of same time, it won't store a double-top if the bottom never levels... which might not be an issue
			if(!empty($this->new_slope)&&$this->new_slope==$last_level['slope']){
				if($this->new_slope=='bull'){
					if($new[$high]>$last_level['level2']){
						unset($this->levels[$last_level_key]);
						$this->levels[$index] = [
							'time'=>$new[$time],	// NOTE: Since we use the new timestamp, overwrites will correctly display over the current candle.
							'close'=>$last[$high],
							'level1'=>$average,
							'slope'=>$this->new_slope,
							'level2'=>$new[$high],
							'type'=>'continued'
						];
					}
				} else if($this->new_slope=='bear'){
					if($new[$low]<$last_level['level2']){
						unset($this->levels[$last_level_key]);
						$this->levels[$index] = [
							'time'=>$new[$time],	// NOTE: Since we use the new timestamp, overwrites will correctly display over the current candle.
							'close'=>$last[$low],
							'level1'=>$average,
							'slope'=>$this->new_slope,
							'level2'=>$new[$low],
							'type'=>'continued'
						];
					}
				}
			}	
		}
		$this->last_slope = $this->new_slope;
		return true;
	}
	
	/**
		Alternate logic for head and shoulders which doesn't rely on "isDouble" of a lop-sided shoulder-line like this:  http://prntscr.com/zy3zhm
		(1) Check if isLevel, this is shoulder 1
		(2) Get all levels prior to this one
		(3) If last level is opposite slope, this is neckline 1
		(4) If next level is same slope and higher(normal)/lower(inverse) than current price, this is a head.
		(5) If next level is opposite slope, this is neckline 2
		(6) If next level is same slope and lower(normal)/higher(inverse) than the head price, this is shoulder 2
		(7) If 2 necklines, 2 shoulders, and 1 head, then isHeadShoulders
		(8) The gradient for the neckline should be somewhat similar to the gradient of the shoulders
	**/
	public function isHeadShoulders($price,$time,$increment,$slope='bull',$inverse=false){
		$level = self::isLevel($time,$slope);
		$shoulders = [];
		$head = null;
		$neckline = [];
		$last_slope = $slope;
		if(false!==$level){
			$levels = array_reverse($this->levels);
			if(!empty($levels)&&count($levels)>=5){
				foreach($levels as $key=>$item){
					// Ignore future
					if($item['time']>$time){
						continue;
					}
					// Itself is always first shoulder.
					if($item['time']==$time){
						$shoulders[] = $item;
						// Don't trust the provided slope over this one I guess
						$last_slope = $slope = $item['slope'];
						continue;
					} else if(!empty($shoulders)) {
						// Must alternate
						if($last_slope==$item['slope']){
							break;
						}
						if($inverse==true){
							if($item['slope']==$slope){
								if($item['level1'] < $shoulders[0]['level2']){
									if(!empty($head) && $item['level2'] < $head['level2']){
										return false;
									}
									if(empty($head)){
										$head = $item;
									}
								} else {
									if(	($item['level1'] <= $shoulders[0]['level2'] && $item['level1'] >= $shoulders[0]['level1'])	||	($item['level2'] >= $shoulders[0]['level1'] && $item['level2'] <= $shoulders[0]['level2'])){
										$shoulders[] = $item;
									}
								}
							} else {
								$neckline[] = $item;
							}
						} else {
							if($item['slope']==$slope){
								if($item['level1'] > $shoulders[0]['level2']){
									if(!empty($head) && $item['level2'] > $head['level2']){
										return false;
									}
									if(empty($head)){
										$head = $item;
									}
								} else {
									if(	($item['level1'] <= $shoulders[0]['level2'] && $item['level1'] >= $shoulders[0]['level1'])	||	($item['level2'] >= $shoulders[0]['level1'] && $item['level2'] <= $shoulders[0]['level2'])){
										$shoulders[] = $item;
									}
								}
							} else {
								$neckline[] = $item;
							}
						}
					}
					$last_slope = $item['slope'];
				}
			}
		}
		if(count($shoulders)==2&&count($neckline)==2&&!empty($head)){
			$shoulder_1 = ['ema'=>$shoulders[0]['level2'], 'time'=>$shoulders[0]['time']];
			$shoulder_2 = ['ema'=>$shoulders[1]['level2'], 'time'=>$shoulders[1]['time']];
			$neck_1 = ['ema'=>$neckline[0]['level2'], 'time'=>$neckline[0]['time']];
			$neck_2 = ['ema'=>$neckline[1]['level2'], 'time'=>$neckline[1]['time']];
			$shoulder_gradient = self::getSimpleGradient($shoulder_1,$shoulder_2);
			$neckline_gradient = self::getSimpleGradient($neck_1,$neck_2);
			// If the shoulder and neckline gradients are both declining on a regular chart, we return true because this is ready to sell (alternatively, on an inverse chart, it must be increasing to buy)
			if($shoulder_gradient < 0){
				if($neckline_gradient < 0){
					if($inverse===false){
						return true;
					} else {
						return false;
					}
				} else {
					// If the shoulders are declining but the neckline isn't, this probably is not a head and shoulders
					return false;
				}
			}
			if($shoulder_gradient > 0){
				if($neckline_gradient > 0){
					if($inverse===false){
						return false;
					} else {
						return true;
					}
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	/**
		A head and shoulders pattern ocurrs when the following patterns are met:
		(1) (a) Get the highest point in the history
			(b) Split the history in half. We'll call these BH and AH (before high, afer high)
			(c) Going backwards on BH:
				(c.1) Proceed (backwards) until a candle closes higher than its predecessor (this is to get to the trough). Now slice the segment from trough to the end.
				(c.2) Get highest point of the new segment. Slice the segment between c.2 and c.1
				(c.3) Get lowest point of the new segment.
			(d) Going forwards on AH:
				Repeat step C going forwards on AH:
			(e) Get the current price
		(2) If c.2 and d.2 are within a degree of variability (ENV 1%) AND a is greater than both c.2 and d.2 by more than this degree of variability, this indicates a head and shoulders pattern could exist (it wouldn't exist if d.2 is the latest price).
		(3) If e is lower than d.3, head and shoulders is confirmed.
		
		For an inverse head and shoulders, flip it and get the reverse results
		
		NOTE: Since we are storing levels properly, we're using those instead of the steps above.
	**/	
	public function isHeadAndShoulders($price,$end,$increment,$slope='bull',$inverse=false){
		// CRITICAL REFACTOR: Checking for a double doesn't quite work if the shoulders are slightly off like this one... we should use the slanted check for that.
		$begin = self::isDouble($price,$end,$slope);
		$slice = $temp = array_reverse($this->levels);
		$head = false;
		if(false!==$begin){
			$begin = $begin['time'];
			// Now that we have hit a double, we must now check to see if there is a higher level between these two, so we are going to slice the levels array between now and then
			foreach($temp as $key=>$level){
				// We don't want the current level nor anything in the future
				if($level['time']>=$end){
					unset($slice[$key]);
				}
				if($level['time']<$begin){
					unset($slice[$key]);
				}
			}
			$neckline = [];
			if(!empty($slice)&&count($slice)>1){
				foreach($slice as $item){
					// A head and shoulders pattern has a neckline, so basically we should see a double bull AND a double bear with a peak in between				
					if($inverse==true){
						if($item['slope']==$slope&&!empty($neckline)){
							// If this level's start is lower than the current price, this is a head.
							if($price > $item['level1'] && $item['time']!==$begin && $item['time']!==$end){
								if(!empty($head)&&$head['level1'] > $item['level1']){
									return false;
								}
								$head = $item;
							}
						} else {
							$prz = ($item['level1']+$item['level2']/2);
							if(false==self::isBreakaway($item['time'],$prz,$increment,$slope)&&false!==self::isAllChart(($item['time']-$increment),$prz,$increment,$slope)&&false!==self::isKicker($item['time'],$increment,$slope)){
								$neckline[] = self::isDouble($prz,$end,$item['slope'],$begin);
							}
						}
					} else {
						if($item['slope']==$slope&&!empty($neckline)){
							// If this level's start is higher than the current price, this is a head.
							if($price < $item['level1'] && $item['time']!==$begin && $item['time']!==$end){
								if(!empty($head)&&$head['level1'] < $item['level1']){
									return false;
								}
								$head = $item;
							}
						} else {
							$prz = ($item['level1']+$item['level2']/2);
							// Any major break away pattern should immediately dismiss a head and shoulders... In theory this should be a really neatly organized pattern.
							if(false==self::isBreakaway($item['time'],$prz,$increment,$slope)&&false!==self::isAllChart(($item['time']-$increment),$prz,$increment,$slope)&&false!==self::isKicker($item['time'],$increment,$slope)){
								$neckline[] = self::isDouble($prz,$end,$item['slope'],$begin);
							}
						}
					}
				}
			}
			if(!empty($head)&&!empty($neckline)){
				return true;
			}
		}
		return false;
	}
	
	/**
		A Kicker is a breakaway pattern.  This makes it the opposite of a Spike in that it looks the same but the opposite action is taken.
		Here you are basically buying a spike before it becomes a spike based on a sharp change of plan.
		(1) Get current candle's price range open-to-close not low-to-high
		(2) If greater than deviation band * 2, proceed
		(3) If the previous candle also had high variability AND is a level (reversal)
	**/	
	public function isKicker($t,$increment,$slope){
		// REFACTOR: This should not be hard-coded
		$prev_band = 9;
		$iterations = 7;
		$count = ($prev_band+$iterations);
		$past_time = ($t-($increment*$count));
		$time_col = $this->time_key;
		$c = $this->close_key;
		$o = $this->open_key;
		$h = $this->high_key;
		$l = $this->low_key;
		$history = self::getHistory($past_time,0,$increment);
		$kicker_band = Config::get('services.algorithm.kicker_band');
		$spike_x = Config::get('services.algorithm.spike_x');
		if(count($history) < $count){
			return false;
		}		
		if(!empty($this->deviation_band[$t])){
			$stagnant = Config::get('services.algorithm.stagnant_threshold');
			$default_deviation = $stagnant/2;	// This is pseudo-science...
			$deviation_multiplier = ($this->deviation_band[$t] < $default_deviation)? ($default_deviation/$this->deviation_band[$t]) : 1;
			$key = array_search($t, array_column($history,$time_col));
			// Returns the key for the current timestamp in the history (which in theory should be the end
			if($key<$count){
				return false;
			}
			$event = $history[$key];
			$high = $event[$h];
			$low = $event[$l];
			$close = $event[$c];
			$open = $event[$o];
			$range = abs($close-$open);
			if($range > ($this->deviation_band[$t]*$spike_x*$deviation_multiplier)){
				return true;
			}
			// In a kicker, the activity before this activity must be low volatility, which is why we use the deviation band so that we don't have to analyze every candle.
			// A kicker is not really a kicker unless the low or high holds with the closing price.
			$driving_price = Config::get('services.algorithm.driving_price');
			$candle_size = (($high-$low)<>0)? ($high-$low) : 0.00000001;
			$xt = ($slope=='bear')? $low : $high;
			$cth_pct = abs(($xt-$close)/$candle_size);	// Take the difference between high or low and close and divide by candle height to see if we are close to the high or low
			if($cth_pct <= $driving_price){
				$tally = $kicker_count = 0;
				$low_tally = 0;
				$flag = false;
				$stagnant = Config::get('services.algorithm.stagnant_threshold');
				// We now check for a continuation pattern backwards.  Before the last candle, the previous 2 candles should be low volatility, low variance.  A kicker should have at least 3 total same slope
				for($i=0; $i<$iterations; $i++){
					$prev = $history[($key-$i)];
					$new_open = $prev[$o];
					$new_close = $prev[$c];
					$new_avg = ($prev[$o]+$prev[$c])/2;
					$new_time = $prev[$time_col];
					$new_range = abs($new_open-$new_close);
					$new_slope = ($new_close > $new_open)? 'bull' : 'bear';
					if($new_slope == $slope){
						if(isset($this->deviation_band[$new_time]) && ($new_range >= ($this->deviation_band[$new_time]))){
							$kicker_count++;
							if($new_range >= max( (($this->deviation_band[$new_time])*$kicker_band), $default_deviation )){
								// REFACTOR: Instead of kickerband maybe we use deviation multiplier plus stagnant??
								$flag = true;
							}
						}
						$tally++;
					} else {
						break;
					}
					if($kicker_count > 2){
						$flag = true;
					}
					if($flag==true && $tally >= 3 && $kicker_count >= 2){
						if(false!==($anc_level = self::isLevel($new_time,$slope))){
							return true;
						} else {
							$ema_end = $history[($key-$i-$prev_band)];
							$ema_avg = ($ema_end[$c]+$ema_end[$o])/2;
							$ema_time = $ema_end[$time_col];
							$variance = abs($ema_avg - $new_avg)/$new_avg;
							$ema_range = abs($ema_end[$c]-$ema_end[$o]);
							if($variance < $stagnant){
								if(isset($this->deviation_band[$ema_time]) && (($this->deviation_band[$t] < $default_deviation) || (($this->deviation_band[$ema_time] * $ema_avg) > $ema_range))){
									$low_tally++;
								}
							}
						}
						if($low_tally>=2){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
		A checkmark is a scenario where a slope is following a pattern upward or downward beyond the last support or resistance level.
		Similar to a kicker but does not concern itself with price variance.
		If current price is above last level of same slope (broke resistance) and last action was sell... Buy (missed the bottom)
		If current price is below last level of same slope (broke support) and last action was buy... Sell (we are screwing up)
		REFACTOR: This needs to be reimagined
	**/
	public function isCheckMark($t,$increment,$slope,$angle){
		// This hasn't been tested properly yet
		return false;
		// For a check-mark to occur, it must be obvious; the macd and slope should be of the same type
		if($slope!==$angle){
			return false;
		}
		if(empty($this->levels)){
			return false;
		}
		$history = self::getHistory(($t-($increment*2)),0,$increment);
		// REFACTOR: This is different on Alpaca
		$h = 2;
		$l = 1;			
		$key = array_search($t, array_column($history,$this->time_key));
		// Returns the key for the current timestamp in the history (which in theory should be 3rd from the end since we started at time minus 2 increments_
		if($key<2){
			return false;
		}		
		$event = $history[$key];
		$high = $event[$h];
		$low = $event[$l];
		$levels = array_reverse($this->levels);
		$support_or_resistance = null;
		foreach($levels as $price=>$level){
			if($level['slope']!==$slope){
				$support_or_resistance = $level;
				break;
			}
		}
		if($support_or_resistance == null){
			return false;
		}
		if($slope == 'bear' && $high < $support_or_resistance['level1']){
			return true;
		} else if($slope == 'bull' && $low > $support_or_resistance['level2']) {
			return true;
		}
		return false;
	}
	
	/**
		The simple gradient is the angle of a slope between points.
	**/
	public function getSimpleGradient(array $point1, array $point2){
		$ema_diff = $point2['ema'] - $point1['ema'];
		if($ema_diff==0){
			return 0;
		}
		return (($point2['time'] - $point1['time'])/$ema_diff);
	}
	
	/**
		getGradient is based on the EMA.
		It takes the current timestamp and deducts X period (ie. 12) * Y increment (ie. 900) to get furthest back point in a slope
		Then it finds the ema price at that level
		IMPORTANT NOTE: You cannot use this function with a current candle or future price because the ema would not exist
	**/
	public function getGradient($time,$period,$increment,$project=false){
		$start = ($project===true)? ($time-(ceil($period/2)*$increment)) : ($time-($period*$increment));
		$point2 = Ema::where(['increment'=>$increment,'band'=>$period,'sign'=>$this->product])->where('time',$time)->first();
		if(!empty($point2)){
			$point2 = $point2->only('time','ema');
		} else {
			return false;
		}
		$point1 = Ema::where(['increment'=>$increment,'band'=>$period,'sign'=>$this->product])->where('time',$start)->first();
		if(!empty($point1)){
			$point1 = $point1->only('time','ema');
		} else {
			return false;
		}
		return self::getSimpleGradient($point1,$point2);
	}
	
	/**
		The price projected on the basis of the gradient
				First we get the gradient at this point, backwards on the minimum period (ie. 12)
				Whatever this value is effectively determines the price to increase/decrease for each time period.
				For example:
					At time 900 price 10 and time 1800, price 15, gradient is 900/5=180
					So, at time 3600, if I'm comparing with the level at 1800 (price 15), the new price should be 
		IMPORTANT NOTE: You cannot use this function with a current candle or future price unless the gradient exits because the gradient needs a stored ema
	**/
	public function projectPrice($x_two,$x_one,$y_one,$increment,$period,$g=null){
		if(!isset($g)){
			$g = self::getGradient($x_two,$period,$increment);
		}
		if($g===false){
			return false;
		}
		$x_diff = $x_two-$x_one;
		// g = x_diff / (y_two - y_one); Solve for y_one
		// x_diff = g * (y_two - y_one);
		// (y_two - y_one) = x_diff / g
		// y_two = (x_diff / g ) + y_one;
		if($g===0){
			return $y_one;
		}
		$final = (($x_diff / $g ) + $y_one);
		if($final<0){
			return 0;
		}
		return $final;
	}
	
	/**
		Performs a projected price check for each the top and bottom level and returns a new projected level.
		IMPORTANT NOTE: You cannot use this function with a current candle or future price because the projectPrice would require a gradient passed to it.
	**/
	public function projectLevel($time,array $last,$increment,$period){
		if(!isset($last['time'])&&!isset($last['level1'])&&!isset($last['level2'])){
			return false;
		}
		$new = $last;
		$new['time'] = $time;
		$new['level1'] = self::projectPrice($time,$last['time'],$last['level1'],$increment,$period);
		$new['level2'] = self::projectPrice($time,$last['time'],$last['level2'],$increment,$period);
		if(empty($new['level1'])||empty($new['level2'])){
			return false;
		}
		return $new;
	}
	
	/**
		Checks whether the current price is a level which matches at least 2 other levels.
		(1) Check if isLevel
		(2) The first result will return the timestamp where this level last occurred, so we need to re-run after the current timestamp.  If it hits again, then it's currently in a triple
		(3) If there is a reversal at this level, it is officially a triple
		NOTE: 	The price must be the previous time's price, so the best we can do with the current price is get the max/min of high/low and open.
				Returns the timestamp of the first level
	**/
	public function isTriple($price=null,$time,$slope=false,$angle=null,$start=null){
		// At one point every candle will drop below its opening at which point the slope is bearish, so when it gets analyzed, it's a level BUT then a minute later it goes back up and now it's bullish AKA not a level anymore
		// so truly we should be checking to see if the past candle is a level
		$level = self::isLevel($time,$slope);
		if(false==$level){
			return false;
		}
		if(empty($price)){
			$price = (($level['level1']+$level['level2'])/2);
		}
		$next = self::enteredLevel($price,$time,$start,$angle);
		if($next!=false){
			$second = self::enteredLevel($price,$next['time'],$start,$angle);
			if(false!==$second){
				return $next;
			}
		}
		return false;
	}
	
	public function isSCurve($price,$t,$slope,$opposite){
		// Here, the price dropped to a previously bullish level, which means indicates a reversal (ie. single support
		// REFACTOR: In order for this to work, the two levels must sandwich 1 higher and 1 lower level
		$start = self::isDouble($price,$t,$slope,$opposite);
		if(empty($start)){
			return false;
		}
		// We are going back from current level
		$levels = array_reverse($this->levels,true);
		$head = false;
		$foot = false;
		$end = null;
		if(!empty($levels)&&count($levels)>=4){
			foreach($levels as $level){
				// Start with current, go to "start"
				if($level['time'] >= $t){
					if($level['time'] == $t){
						$end = $level;
					}
					continue;
				}
				// It should not be possible for "end" to be empty unless there are no levels at the current time which also shouldn't happen since we have to check if isDouble
				// and isDouble requires isLevel.
				if($level['time'] <= $start['time']){
					break;
				}
				if(!empty($end)){
					// If starts with bull, moves to bear, we expect this to reverse to bull, therefore buy
					if($opposite=='bull'){
						// The end (now) level1 should ideally be higher than the start level2 (start)
						if($end['level1'] < $start['level2']){
							return false;
						}
						// If the level before now clears above and no foot exists
						if($level['level1'] > $end['level1'] && $foot == false){
							$head = true;
						}
						if($level['level1'] < $end['level2'] && $head == true){
							$foot = true;
						}
					}else {
						// The end (now) level2 should ideally be lower than the start level1 (start)
						if($end['level2'] > $start['level1']){
							return false;
						}					
						// If the level before now the clears below
						if($level['level1'] < $end['level1'] && $head == false){
							$foot = true;
						}
						if($level['level1'] > $end['level2'] && $foot == true){
							$head = true;
						}
					}
				}
			}
		}
		if($head==true&&$foot==true){
			return true;
		}
		return false;
	}
	
	/**
		Checks for a double top or bottom
		Returns the timestamp of the first level
	**/
	public function isDouble($price,$time,$slope=false,$angle=null,$start=null){
		$prev = self::enteredLevel($price,$time,$start,$angle);
		if($prev!=false){
			$level = self::isLevel($time,$slope);
			if(false!==$level){
				return $prev;
			}
		}
		return false;
	}

	public function getMACD($bands,$start=null,$increment=null){
		$lower = min($bands);
		$upper = max($bands);
		$short = self::getEMA($lower,$start,$increment);
		$long = self::getEMA($upper,$start,$increment);
		if(empty($short)||empty($long)){
			return [];
		}
		$flag = false; // false indicates short is below long (sell)
		$signals = [];
		$prevS = null;
		$prevL = null;
		$margin = 0;
		$first_time = array_key_first($short);
		$first_price = $short[$first_time];
		$threshold = Config::get('services.algorithm.band_thickness')/2;
		$rtts = Macd::where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$this->product])->where('time','>=',$first_time)->get();
		foreach($short as $t=>$s){
			$flag2 = $flag;
			$rtt = null;
			if(!empty($rtts)){
				if($rtts->contains('time',$t)){
					$rtt = $rtts->where('time',$t)->first();
				}
			}
			if(!empty($rtt)){
				$signals[$t] = $rtt->price;
				$prevS = $s;
				if(isset($long[$t])){
					//$flag = ($long[$t]<$s) ? true : false;
					if(($long[$t]+($long[$t]*$threshold))<$s){
						// If the price is higher than the higher edge of the thick long band
						$flag = true;
					} elseif(($long[$t]-($long[$t]*$threshold))>$s){
						// If the price is lower than the lower edge of the thick long band
						$flag = false;
					}
					$prevL = $long[$t];
				}
				continue;
			}
			if(isset($long[$t])){
				//$flag = ($long[$t]*<$s) ? true : false;
				if(($long[$t]+($long[$t]*$threshold))<$s){
					// If the price is higher than the higher edge of the thick long band
					$flag = true;
					$act = 'bull';
				} elseif(($long[$t]-($long[$t]*$threshold))>$s){
					// If the price is lower than the lower edge of the thick long band
					$flag = false;
					$act = 'bear';
				}
				$s_key = !empty(array_key_last($signals))? array_key_last($signals) : $first_time;
				if(!empty($prevL)&&$flag!==$flag2){
					$signals[$t] = $s;
					$rtt = new Macd();
					$rtt->sign = $this->product;
					$rtt->time = $t;
					$rtt->price = $s;
					$rtt->action = $act;
					$rtt->bands = $lower.'-'.$upper;
					$rtt->increment = $increment;
					$ps = isset($signals[$s_key])? $signals[$s_key] : $first_price;
					$rtt->signal_signal_margin = number_format(($s-$ps), 14);			// Margin from now to previous signal
					if($this->save_results!==false){
						$rtt->save();
					}
					$history = self::getHistory($t,0,$increment);
				}
				$prevL = $long[$t];
			}
			$prevS = $s;
		}
		return $signals;
	}
	
	/**
		Gets all actions regardless of status and merges them with macd, to plot on a single line
		// REFACTOR: Right now this assumes the macd has been run already
	**/
	public function getActions($bands,$start=null,$increment=300){
		$lower = min($bands);
		$upper = max($bands);
		// By default this API returns 300 units of results.  We'll take the now time... doesn't need to be exact, we're just getting historic actions.
		if($start==null){
			$now = time();
			$start = $now-($increment*300);
		}
		$last = Action::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$this->product])->where('time','>=',$start)->first();
		// We don't want to revise history, just start from the last recorded one of any status.
		$history = self::getHistory(!empty($last)? $last->time : $start,0,$increment);
		if(!empty($history)){
			foreach($history as $event){
				$replace = false;
				if(!isset($last) || (isset($event[$this->time_key]) && $event[$this->time_key] >= $last->time)){
					// Replace the most recent or any new one.
					$replace = true;
				}
				$action = self::getAction($event,$bands,$increment,$replace);
			}
		}
		$actions = Action::where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$this->product])->where('time','>=',$start)->get();
		if(!empty($actions)){
			$actions = $actions->keyBy('time')->toArray();
			$macd = Macd::where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$this->product])->where('time','>=',$start)->get();
			if(!empty($macd)){
				$macd = $macd->keyBy('time')->toArray();
				$actions = array_replace($macd,$actions);
			}
			ksort($actions);
			return $actions;
		}
		return false;
	}
	
	/**
		Gets the preceding level of optional type from prior to a given timestamp.
	**/
	public function getPrecedingLevel($t,$slope=null,$ignore=false){
		// We have to traverse the levels backward to find the last level of given slope type.
		if(empty($this->levels)){
			return false;
		}
		$levels = array_reverse($this->levels,true);
		foreach($levels as $past){
				// Ignore current and future levels
				if(empty($past)||$past['time']>=$t){
					continue;
				}
				// Ignore contination levels
				if($ignore==true && $past['type']=='continued'){
					continue;
				}
				// Now that we are onto past levels, skip anything that doesn't match the given slope, if provided.
				if(!empty($slope)&&$slope!==$past['slope']){
					continue;
				}
				// If we are not matching slope, return first available. If we are, return first available.
				if($past['time']!=$t){
					return $past;
				}
		}
		return false;
	}
	
	/**
		Gets the last level of similar slope and checks to see if the current time and price could be considered a double level if we considered the angle of the slope.
	**/	
	public function isSlantedDouble($t,$price,$increment,$slope,$period){
		$result = false;
		if(false!==self::isLevel($t,$slope)){
			$last_level = self::getPrecedingLevel($t,$slope);
			if(empty($last_level)){
				return false;
			}
			$projection = self::projectLevel($t,$last_level,$increment,$period);
			if($projection==false){
				return false;
			}
			if($slope=='bull'){	// In theory, this current point would either be a bull with a breakthrough or a bear at a level
				// Levels don't often hit exactly but can be within a small degree of variance, so we are using the average instead of the close price
				if($price <= $projection['level2'] && $price >= $projection['level1']){
					$result = $projection;
				}
			} else if($slope=='bear'){
				// Levels don't often hit exactly but can be within a small degree of variance, so we are going to shift the close price up to the average
				if($price >= $projection['level2'] && $price <= $projection['level1']){
					$result = $projection;
				}
			}
		}
		return $result;
	}
	
	public function isSpikeReversal($t,$price,$last_price,$last_deviation,$increment,$slope,$angle){
		$result = false;
		// If the preceding deviation was extreme low volatility, the spike_x value should be multiplied. 
		// Stagnation is 2x fee (ie. 0.008) which is 4x deviation (ie. deviation is 0.002).  Anything below is too thin to rely upon for spike_x alone.
		// CRITICAL CRITICAL REFACTOR: This hasn't been thoroughly tested.
		$stagnant = Config::get('services.algorithm.stagnant_threshold');
		$default_deviation = $stagnant/2;	// This is pseudo-science...
		$deviation_multiplier = ($last_deviation < $default_deviation)? ($default_deviation/$last_deviation) : 1;
		$flag = false;
		// If the evaluated candle or its predecessor are levels, take action.  To avoid problems with long candles or 5m candles which can afford to wait, we do not evalute this candle for those.
		if(false!==($lvl = self::isLevel(($t-$increment),$slope))){
			$flag = true;
			$price = $lvl['level2'];			
		}
		if(false!==($lvl = self::isLevel($t,$slope)) && $increment == 900){ // CRITICAL REFACTOR: This should be a setting.
			$flag = true;
			$price = $lvl['level2'];
		}
		// If the macd is bullish, we're crossing the upper bollinger on a spike and ready to sell.
		$bg = ($angle=='bull') ? 'bollinger_upper' : 'bollinger_lower';
		if($flag==true){
			// If this is an all-time high or low, take action
			if(false!==self::isAllChart(($t-$increment),$price,$increment,$angle)){
				if(($angle=='bear' && $price<=($this->$bg[($t-$increment)])) || ($angle=='bull' && $price>=($this->$bg[($t-$increment)]))){
					$result = true;
				}
			}
			// If this exceeds a threshold that would sound an alarm (ie. a 3% drop or increase without any redirection in the process)
			$spike_x = Config::get('services.algorithm.spike_x');
			$spike_pct = Config::get('services.algorithm.spike_pct');
			if( $spike_x !== false && isset($this->deviation_band[$t-$increment]) ){
				if( (($spike_x * $deviation_multiplier * $last_deviation) < ($this->deviation_band[$t-$increment])) || (($spike_x * $last_deviation) < ($this->deviation_band[$t])) ){
					$result = true;
				}
			}
			if( $result == true && $spike_pct !== false ){
				if( $spike_pct > (abs($price-$last_price)/min($price,$last_price)) ){
					$result = false;
				}
			}
		}
		return $result;
	}
	
	/**
		getAction will only get the action for a given range and increment at a given time.
	**/
	public function getAction($event,array $bands,$increment=300, $replace=false){
		$lower = min($bands);
		$upper = max($bands);
		$t = $event[0];
		$prev_t = $t-$increment;
		$lp = $event[1];
		$hp = $event[2];
		$op = $event[3];
		$cp = $event[4];
		$ap = ($lp+$hp)/2;
		$pid = $this->product;
		$status = null;
		$action_price = $cp;
		$ema_long = 'ema_'.$upper;
		$ema_short = 'ema_'.$lower;
		$limit_method = $order_type = null;
		if((Config::get('app.env')!=='live'&&Config::get('app.env')!=='production')){
			$action_price = $cp;
		}
		$dev = Config::get('services.algorithm.band_thickness');
		// CRITICAL REFACTOR: When looking at an existing candle, is the closing price the current price??
		$slope = ($op<$cp)?'bull':'bear';
		$opposite = ($slope=='bear')? 'bull' : 'bear';
		$macd = Macd::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])->where('time','<=',$t)->first();
		
		$action = Action::where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])->where('time',$t)->first();
		
		// REFACTOR: Should I add rejected here too?
		if(!empty($action)&&((!empty($action->status) && ($action->status!='wait') && ($action->status!='cancelled'))||$replace===false)){
			return $action->action;
		}
		
		$last_recorded_time = Action::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])->first();
		if(!empty($last_recorded_time)){
			$last_recorded_time = $last_recorded_time->time;
			// Ok, so now if the current candle being evaluated is before the last recorded time, we ignore it.
			// We have only gotten here if the action is empty (so returning false is fine) OR
			// The "replace" param is true, which would only happen for the most recent candle, so checking for "<" is fine.
			// 	OR The action is not empty but the status is empty, in which case this is an open order
			//  If it is an open order, it should be the most reccent time-wise and therefoe pass this step anyway.
			//  However, if it is not the most recent, the order processor will ignore it unless most recent is a "wait" (can still execute) or it'll be cancelled.  The "wait" action would have overridden it though.
			if($t < $last_recorded_time){
				return false;
			}
		}
		
		// This will only return the last done or open action, it won't return cancelled/rejected actions
		$last = Action::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
					->where('time','<',$t)
					->where(function ($q){
						$q->where('status','=','done')
						->orWhere('status','=','open')
						->orWhere('status','=','pending')
						->orWhere('status','=','wait')
						->orWhereNull('status');
					})->first();

		if(!isset($this->bollinger_lower[$t])||!isset($this->bollinger_upper[$t])){
			return false;
		}
		$reason = $pattern = [];
		// When we get a gradient, we have to give it a past time because the current candle hasn't finished (the ema isn't done)
		$g = self::getGradient(($t-$increment),$lower,$increment);
		$g_proj = self::getGradient(($t-$increment),$lower,$increment,true);
		// This is a projection of where the price could go if the pattern continued but of course the idea is that we take action when the pattern does not continue, so it is realy only used to calculate the slope/variance
		$proj = self::projectPrice(($t+($lower*$increment)),$t,$ap,$increment,$lower,$g);
		$proj_proj = self::projectPrice(($t+($lower*$increment)),$t,$ap,$increment,$lower,$g_proj);
		$proj = !empty($proj)? $proj : $ap;
		$proj_proj = !empty($proj_proj)? $proj_proj : $proj;
		//	REFACTOR: Is there a way of determining sideways behavior without a setting?
		$stagnant = Config::get('services.algorithm.stagnant_threshold');
		$variance = ($proj-$ap)/$ap;
		$variance = ($variance<>0)? $variance : 0.00001;
		// In tight patterns, the variance (the gradient) will be mostly flat-lined even though there are waves of activity, which could result in break-patterns.
		// To avoid this, we use a projected variance which builds a gradient not off a trailing short-ema but by using half of the trailing ema to build a projected ema.
		$projected_variance = ($proj_proj-$ap)/$ap;
		$projected_variance = ($projected_variance<>0)? $projected_variance : 0.00001;
		$broken = false;
		// SPIKE ACTIONS
		if(!empty($this->levels)){
			if(false!==self::isKicker($t,$increment,$slope)){
				$pattern[] = $reason[] = 'Kicker';
				$order_type = 'market';
				if($slope=='bull'){
					$act = 'buy';					
				} else {
					$act = 'sell';
				}
				$broken = true;
			}
			if(!empty($macd)){
				if(false!==self::isCheckMark($t,$increment,$slope,$macd->action)){
					$reason[] = 'CheckMark';
					$broken = true;
				}
			}
			if(!empty($macd)&&$macd->action!==$slope){
				// Preceding level check is an match lookup check whereas isLevel is opposite check of a specific time.
				if(!empty($last) && $last->status=='done'){
					$preceding_price = $last->price;
					$preceding_deviation = isset($this->deviation_band[$last->time])? $this->deviation_band[$last->time] : null;
					$comparison_price = ($macd->action=='bull')? $hp : $lp;
					$spike_trigger = false;
					// We are following a trigger action
					if(false!==($prv = self::getPrecedingLevel($t,$opposite))){
						$spike_trigger = true;
					}
					// We are currently in a trigger level
					if(false!==($prv_2 = self::isLevel($t,$slope))){
						if(false===$prv){
							$prv = $prv_2;
							$prv_2 = false;
						}
						$spike_trigger = true;
					}
					if(false===($preceding_level = self::isLevel(($t-($increment*2)),$opposite))){
						if(false!==($preceding_level = self::getPrecedingLevel(($t-($increment*2)),$slope))){
							$preceding_price = ($macd->action=='bull')? min($preceding_price,$preceding_level['level2']) : max($preceding_price,$preceding_level['level2']);
							if(!empty($preceding_deviation)){
								$preceding_deviation = min($preceding_deviation,$this->deviation_band[$preceding_level['time']]);
							} else {
								$preceding_deviation = $this->deviation_band[$preceding_level['time']];
							}
						}
					} else {
						$preceding_price = ($macd->action=='bull')? min($preceding_price,$preceding_level['level2']) : max($preceding_price,$preceding_level['level2']);
						if(!empty($preceding_deviation)){
							$preceding_deviation = min($preceding_deviation,$this->deviation_band[$preceding_level['time']]);
						} else {
							$preceding_deviation = $this->deviation_band[$preceding_level['time']];
						}
					}
					if($macd->action=='bear' && $slope=='bull'){
						if($spike_trigger == true){
							if(false!==$prv_2){
								if($prv_2['level2'] < $prv['level2']){
									$prv = $prv_2;
								}
							}
							$comparison_price = isset($comparison_price)? min($comparison_price,$prv['level2']) : $prv['level2'];							
							if($prv['time']!==($last->time) && isset($this->$ema_short[$t]) && $action_price < $this->$ema_short[$t] && $action_price < $preceding_price){
								if(false!==self::isSpikeReversal($t,$comparison_price,$preceding_price,$preceding_deviation,$increment,$slope,$macd->action)){
									$pattern[] = $reason[] = 'Spike and Reversal';
									$status = 'wait';
									$order_type = 'market';
									$broken = true;
								}
							}
						}
						$act = 'buy';
					} else if($macd->action=='bull' && $slope=='bear'){
						// If the macd crossover is still bullish and the price has raised to a level or is triggered by a pattern, it is a sell action
						if($spike_trigger == true){
							if(false!==$prv_2){
								if($prv_2['level2'] > $prv['level2']){
									$prv = $prv_2;
								}
							}
							$comparison_price = isset($comparison_price)? max($comparison_price,$prv['level2']) : $prv['level2'];
							if($prv['time']!==($last->time) && isset($this->$ema_short[$t]) && $action_price > $this->$ema_short[$t] && $action_price > $preceding_price){
								// Never buy the spike on the current candle.  That's a falling knife.
								if(false!==self::isSpikeReversal(($t-$increment),$comparison_price,$preceding_price,$preceding_deviation,$increment,$slope,$macd->action)){
									$pattern[] = $reason[] = 'Spike and Reversal';
									$status = 'wait';
									$order_type = 'market';
									$broken = true;
								}
							}
						}
						$act = 'sell';
					}
				}
			}
		}
		
			// TOUGH AND CREST LOGIC
			if(!empty($this->levels)){
				$scenario = 0;
				$continuation_price = false;
				$spiking = false;
				if(!empty($last) && $last->status=="done" && false !== strpos(strtolower($last->reason),'break')){	// Last action was a break
					$continuation_price = ($last->action == 'sell')? ($action_price < $last->price) : ($action_price > $last->price);	// The trend continues (true or false)
				}
				if(false!==($lvl = self::isLevel(($t-$increment),$slope)) && false!=self::isLevel($t)){
					$scenario = 1;	// Sideways Reversal
				} else if(false!==($lvl = self::isLevel($t,$slope)) && $increment > 300 && ($lvl['type'] != 'continued' || $lvl['type']==='crossed')){	// No reason to be risky with short candles.				
					if($continuation_price==false){	// If not a continued trend following a break
						$scenario = 2;	// Current Reversal
						if($lvl['type']==='crossed'){	// Crossed resistance (ie. bull crossed upper or bear crossed lower)
							// $spiking = true;	// Break Pattern (Not a Reversal) CRITICAL CRITICAL CRITICAL : Is this supposed to be a spike or not? Shouldn't kickers et al address this?
							$broken = true;		// Break Pattern (Not a Reversal) 
						}
					}
				} else if((false!==($lvl = self::getPrecedingLevel($t,null,true))) && ($opposite==$lvl['slope']) && (empty($last) || $lvl['time'] > $last->time)){
					if($continuation_price==false){
						$scenario = 3;	// Stepping Reversal
					}
				}
				if(false!==($tmp = self::isLevel($t)) && $tmp['slope'] === $slope){	// Is Level Override
					$scenario = 0;	// If a current nonsensical level (ie. Now bullish but also a bull slope (a bull candle would sit atop a bear slope).
				}
				$steep = (abs($variance) > ($stagnant*2));
				$gradual = (abs($variance) < $stagnant) ? true : false;
				$flat = (abs($projected_variance) < ($stagnant/2))? true : false;
				$thick = (($this->deviation_band[$t] / $ap ) >= $stagnant)? true : false;
				$thin = (($this->deviation_band[$t] / $ap ) < $stagnant)? true : false;
				$normal = (($this->deviation_band[$t] / $ap ) > $this->fee)? true : false;
				if(!isset($this->bollinger_lower[$t]) || !isset($this->bollinger_upper[$t])){
					$this->bollinger_lower[$t] = $this->bollinger_lower[$t-$increment];
					$this->bollinger_upper[$t] = $this->bollinger_upper[$t-$increment];
				}
				if($scenario>0){
					$flag = false;					
					$band_thickness = Config::get('services.algorithm.band_thickness');
					$variance_fee = ( $action_price * (($this->fee) / 2) ) + abs(($action_price * $variance) / 2) ;
					$ema = $ema_short;
					$ema_price = isset($this->$ema[$t])? $this->$ema[$t] : $action_price;
					$delta = abs($action_price - $ema_price);
					$feeable = false;
					if(isset($this->$ema[$t])){
						$feeable = ($slope=='bull') ? (($action_price + $variance_fee) < $this->$ema[$t]) : (($action_price - $variance_fee) > $this->$ema[$t]);
					}
					if($slope=='bull'){
						$profitable = (!empty($last) && $last->action=='sell' && (($last->price - $action_price)/$last->price) >= ($this->fee*2) ) ? true: false;
					} else {
						$profitable = (!empty($last) && $last->action=='buy' && (($action_price - $last->price)/$last->price) >= ($this->fee*2) ) ? true : false;
					}
					$wall = ($slope=='bull')? ($lp < $this->bollinger_lower[$t] && $action_price > $this->bollinger_lower[$t]) : ($hp > $this->bollinger_upper[$t] && $action_price < $this->bollinger_upper[$t]);
					if(isset($this->$ema_short[$t])){
						$action_over_short = ($action_price >= $this->$ema_short[$t])? true : false;
						$action_under_short = ($action_price <= $this->$ema_short[$t])? true : false;
					}
					if(in_array('Spike and Reversal',$reason) || in_array('Kicker',$reason) || in_array('checkMark',$reason)){
						$spiking = true;
					}
					if(!empty($last) && !$broken){
						if(!$wall && (($thick && !$gradual) || ($action_price > $this->bollinger_upper[$t] || $action_price < $this->bollinger_lower[$t]))){
							$broken = ($slope == 'bull')? ($last->action=='sell' && $last->price < $action_price) : ($last->action=='buy' && $last->price > $action_price);
						}
					}
					if(in_array('Spike and Reversal',$reason)&&( ( $slope=='bull' && $action_over_short ) || ( $slope=='bear' && $action_under_short ) )){
						$spiking = false;
						$spike_key = array_search('Spike and Reversal', $reason);
						unset($reason[$spike_key]);
					}
					$crosses = false;
					if(isset($this->$ema_long[$t]) && isset($this->bollinger_lower[$t]) && isset($this->bollinger_upper[$t])){
						if($this->bollinger_lower[$t] > $this->$ema_long[$t] || $this->bollinger_upper[$t] < $this->$ema_long[$t]){
							$crosses = true;
						}
					}
					$ema = ($steep || $thick || $crosses)? $ema_short : $ema_long;
					if(!isset($this->$ema[$t]) && isset($this->$ema[$t-$increment])){
						$this->$ema[$t] = $this->$ema[$t-$increment];
					}
					if($spiking == true && !empty($macd)){
						$flag = true;
						$limit_method = 'any';							
					}
					if($lvl['slope']=='bear' && $slope=='bull'){
						if($gradual && $thin){	// Stagnant Waves
							if($feeable || $profitable){	// Won't Lose Money
									if($scenario === 1){	// Last Candle Was Level
										if((!empty($macd) && $macd->action == $slope) || $flat){
											$flag = true;		// Always take a supporting action & If it is nearly flat, all is fair game.
										} else if($this->$ema_short[$t] > $action_price) {
											$status = 'wait';	// Not supported AND Not Flat
											$flag = true;
										} else if($this->$ema_long[$t] > $action_price) {
											$flag = true;	// Not supported AND Not Flat
										}
									}
									if($scenario === 2 && $flat && !$broken && $wall && $normal){	// Ignore crossing candles.
										$flag = true;
										$status = 'wait';	// Just to be sure this doesn't fall through (ie. candle swap)
									}
							}
							if($flag === true && !empty($last) && $last->action==='sell' && $last->status==='done' && (abs(($action_price - $lvl['level2'])/$lvl['level2']) < ($this->fee))){
								$limit_method = empty($limit_method)? 'post_only' : $limit_method;
							}
						} else if($scenario === 2){
							// Do not buy on a crossing level during any sort of volatility.  If it deserves buying, a Kicker will fire it.
						} else if($thick) {  // Scenario 1 or 3
							if(isset($this->bollinger_lower[$lvl['time']]) && $lvl['level2'] < $this->bollinger_lower[$lvl['time']] && ($lp > $this->bollinger_lower[$t] || $lp > $this->bollinger_lower[$t-$increment])){
								$spiking = true;
								$flag = true;
								$limit_method = 'any';
								$status = null;
								$reason[] = 'Bollinger Break';
							}
							if($increment >= 900 || $spiking || $crosses ){
								// In volatile waves, take action whenever the slope supports it (ie. if bullish, buy at the candle).  We use a wait here just to confirm.
								if(!empty($macd) && $macd->action===$slope){		
									$flag = true;
									$limit_method = 'any';
									if((time() < ($t+($increment/5))) || $spiking == true){ // Unlike selling, spikes are a falling knife, so we defer.  If back-half of the candle, it's safe
										$status = 'wait';
									}
								}
								// In all cases, take action based on EMA crossings
								if($normal && isset($this->$ema[$t]) && ( ($action_price -($action_price*$band_thickness)) >= ($this->$ema[$t]) )){
									$flag = true;
									$status = null;
								}
							}
						}
						if($flag==true){							
							//if(false!==self::isHeadAndShoulders($lp,$t,$increment,$slope,true)){
							if(false!==self::isHeadShoulders($lp,$lvl['time'],$increment,$slope,true)){
								$reason[] = 'Inverse Head and Shoulders';
								// We use AP here because we use averages for evaluating entered levels
							} else if(!empty($macd)&&false!==($prvl = self::isSlantedDouble($lvl['time'],$ap,$increment,$macd->action,$lower))){
								$reason[] = 'Double Bottom off EMA Gradient';
							} else if(!empty($macd)&&false!==self::isTriple(null,$lvl['time'],$slope,$macd->action)){
								$reason[] = 'Triple Bottom';
							} else if(!empty($macd)){
								// We use AP here because if we didn't, it would fail the double support test
								$prvl=self::isDouble($ap,$lvl['time'],$slope,$macd->action);
								// Here, the price entered a level and created a level both in the same direction, hence support
								if(false!==$prvl && $lp > $prvl['level1']){
									$reason[] = 'Double Support Level';
								} else if(false!==self::isSCurve($lp,$lvl['time'],$slope,'bull')){
								// Here, the price dropped to a previously bearish level, which means indicates a reversal (ie. support)
									$reason[] = 'Support off Previous Resistance Level';
								}
							}
							if($status != 'wait'){
								$limit_method = 'any';
							}
							$act = 'buy';
							switch($scenario){
								case 1:
									$reason[] = "Sideways Trailing Level";
									break;
								case 2:
									$reason[] = "Current Crossing Level";
									break;
								case 3:
									$reason[] = "Stepping Reversal Forming";
									break;
								default:
									$reason[] = "Undefined Break";
									break;
							}
							$pattern[] = 'Trough';	
						}
					// A bull level indicates the end of a bull pattern, therefore the level slope is a bull and currently the slope is bearish (sell)
					// We try not to sell a rising pattern... We would only do that if the MACD hasn't crossed yet but is headed that direction (closing price is below the long ema) OR if it's a sideways pattern
					} else if($lvl['slope']=='bull' && $slope=='bear'){
						if($gradual && $thin){	// Stagnant Waves
							if($feeable || $profitable){	// Won't Lose Money
									if($scenario === 1){	// Last Candle Was Level
										if((!empty($macd) && $macd->action == $slope) || $flat){
											$flag = true;		// Always take a supporting action & If it is nearly flat, all is fair game.
										} else if($this->$ema_short[$t] > $action_price) {
											$status = 'wait';	// Not supported AND Not Flat
											$flag = true;
										} else if($this->$ema_long[$t] > $action_price) {
											$flag = true;	// Not supported AND Not Flat
										}
									}
									if($scenario === 2 && $flat && !$broken && $wall && $normal){	// Ignore crossing candles.
										$flag = true;
										$status = 'wait';	// Just to be sure this doesn't fall through (ie. candle swap)
									}
							}
							if($flag === true && !empty($last) && $last->action==='buy' && $last->status==='done' && (abs(($action_price - $lvl['level2'])/$lvl['level2']) < ($this->fee))){
								$limit_method = empty($limit_method)? 'post_only' : $limit_method;
							}
						} else if($scenario === 2){
							if($broken && $feeable && in_array('Spike and Reversal',$reason)){
								// Spike and Reversals always sell in Scenario 2 and will trigger on their own but we want this for tracking and to help remove teh wait.
								$flag = true;
								if(time()>($t+($increment/3)) || (!empty($macd) && $macd->action != $slope)){
									$status = null;
								}
							}
						} else if($thick) {
							if($lvl['level2'] > $this->bollinger_upper[$lvl['time']] && ($hp < $this->bollinger_upper[$t] || $hp < $this->bollinger_upper[$t-$increment])){
								$spiking = true;
								$flag = true;
								$limit_method = 'any';
								$status = null;
								$reason[] = 'Bollinger Break';
							}
							if($increment >= 900 || $spiking || $crosses ){
								// In non stagnant waves, take action whenever the slope supports it (ie. if bullish, buy at the candle)
								if(!empty($macd) && $macd->action===$slope){
									$flag = true;
									$limit_method = 'any';
									if((time() > ($t+($increment/5))) || $spiking == true){
										// Do not wait
									} else {
										$status = 'wait';
									}
								}
								if($normal && isset($this->$ema[$t]) && ( $action_price < ($this->$ema[$t]) )){
									$flag = true;
									$status = null;
								}
							}
						}
						if($flag==true){
							// NOTE: We used to pass $lp here but now we pass null because isTriple will take the average of the levels
							if(false!==self::isHeadShoulders($hp,$lvl['time'],$increment,$slope,false)){
								$reason[] = 'Head and Shoulders';
								// We use AP here because we need to use APs for projections
							} else if(!empty($macd)&&false!==($prvl = self::isSlantedDouble($lvl['time'],$ap,$increment,$macd->action,$lower))){
								$reason[] = 'Double Top off EMA Gradient';
							} else if(!empty($macd)&&false!==self::isTriple(null,$lvl['time'],$slope,$macd->action)){
								$reason[] = 'Triple Top';
							} else if(!empty($macd)){
								// We use AP here because if we didn't, it would fail the double resistance test
								$prvl=self::isDouble($ap,$lvl['time'],$slope,$macd->action);
								if(false!==$prvl && $hp < $prvl['level1']){
									$reason[] = 'Double Resistance Level';
								} else if(false!==self::isSCurve($hp,$lvl['time'],$slope,'bear')){
									// Here, the price dropped to a previously bullish level, which means indicates a reversal (ie. resistance)
									$reason[] = 'Resistance off Previous Support Level';
								}
							}
							if($status != 'wait'){
								$limit_method = 'any';
							}
							$act = 'sell';
							switch($scenario){
								case 1:
									$reason[] = "Sideways Trailing Level";
									break;
								case 2:
									$reason[] = "Current Crossing Level";
									break;
								case 3:
									$reason[] = "Stepping Reversal Forming";
									break;
								default:
									$reason[] = "Undefined Break";
									break;
							}
							$pattern[] = 'Crest';
						}
					}
				}
			}
			if(!empty($last)&&!empty($this->levels)&&false!==strpos(strtolower($last->reason),'spike and reversal')){
				$lvl = self::getPrecedingLevel($t,$opposite);
				// If the last action reason was a Spike and Reversal, reverse the charge at the next supporting level.  Sometimes this is premature, so we may wait for a better price.
				$compare_price = null;
				if(false!==$lvl && $lvl['time'] > $last->time){
					// Get whichever level occurred before the spike reversal.  A spike reversal is triggered on the previous timestamp, so we will look before then.
					$previous = self::getPrecedingLevel(($last->time)-($increment*2));
					if(false!==$previous){
						if($previous['slope']!==$opposite){
							// If for some reason there is no supporting level (level is wrong type), we'll use its level1.
							$compare_price = $previous['level1'];
						} else {
							$compare_price = $previous['level2'];
						}
						if($last->action=='buy' && $slope==='bear'){
							$act = 'sell';
							// We wait on a bullish pattern (don't sell if it'll keep going up).  In a bullish pattern, this level2 would be higher than last level2, so we wait
							if($compare_price < $lvl['level2'] || $increment <= 900){
								$status = 'wait';
							}
							$pattern[] = $reason[] = 'Catching Post-Spike';
						}
						if($last->action=='sell' && $slope==='bull'){
							$act = 'buy';
							// We wait on a bearish pattern (don't buy if it will keep going down).  In a bearish pattern, this level2 would be lower than the last level2, so we wait
							if($compare_price > $lvl['level2'] || $increment <= 900){
								$status = 'wait';
							}
							$pattern[] = $reason[] = 'Catching Post-Spike';
						}
					}
				}
			}
			// If no action is occurring and we already took an action
			// AND the current time is not a level OR is a supporting level
			// Remember, a buy level is a bear slope because it is the end of the bear run, so checking isLevel with 'bear' would be false
			// Break patterns should be ignored on sideways behavior.  We reduce stagnation because a breakaway will develop beyond the existing variance and quickly
			// So for example, if the breakaway breaks upward, deviation would double but variance wouldn't establish for another 9 units.
			// If the last action was a sale and the price is driving higher, beyond the bollinger band, we need to re-buy before it is too late
			// If the last action was a purchase and the price is driving downward, we need to sell before it is too late.
			// Last action  must be at least 2x$increment to account for scenarios where a the action occurs near/over bollinger and only 1 candle beyond that occurs.
			// In order to be a break away, the candle must either (a) proceed past the bollinger multiple times or (b) exceed an ATH or ATL in the current chart
			
			// BREAK-AWAY PATTERNS
			$supporting_level = false;
			if(false===self::isLevel($t)){
				$supporting_level = true;
			} else if(false!==self::isLevel($t,$slope) || false!==self::isLevel(($t-$increment),$slope)){
				$supporting_level = true;
			} else if(false!==($tmp==self::isLevel($t,$opposite))){
				if($tmp['type'] == 'continued'){
					$supporting_level = true;
				}
			}
			if(empty($pattern)&&!empty($this->levels)&&!empty($last)&&($supporting_level)){
				if($last->status=='done' && ((!$flat && $thick) || $broken) ){
					$flag = false;
					$tally = 0;
					if(($last->action==="sell" && $slope==='bull') || ($last->action==="buy" && $slope==='bear')){
						$trailing_history = self::getHistory(($t-($increment*4)),$t,$increment);
						foreach($trailing_history as $trailing_event){
							$sub_direction = ($trailing_event[$this->close_key] > $trailing_event[$this->open_key])? 'bull' : 'bear';
							// REFACTOR: Do we care about the candle size?
							if($sub_direction === $slope){
								$tally++;
							}
						}
						if($tally>=3){
							$broken = true;
						}
					}
					// CRITICAL REFACTOR: This seems like a stupid way of doing this... 
					$stop_loss = ((abs($variance) >= $stagnant) || ((abs($variance) < $stagnant)&&(abs($action_price - $last->price)/$last->price) > ($stagnant/2)))? true : false;
					if($broken==true || ( ($stop_loss || $normal) && ($last->time) < $prev_t) ){
						$comparison_price = $last->price;
						$comparison_time = $last->time;
						// The last action should have an anti-slope. If I bought it would have been a bear, so if I'm a bear, I send it a bull to get true
						$lvl = self::isLevel($last->time,$opposite);
						if(false===$lvl){
							if(false!==($lvl = self::getPrecedingLevel($last->time))){
								if($lvl['slope'] === $slope){
									$comparison_price = $lvl['level2'];
								} else {
									$comparison_price = $lvl['level1'];
								}
								$comparison_time = $lvl['time'];
							}
						} else {
							$comparison_price = $lvl['level2'];
							$comparison_time = $lvl['time'];
						}
						$dff = abs($comparison_price-$action_price);
						$previous = Action::orderBy('time','DESC')
										->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
										->where('time','<',$last->time)
										->where('status','done')
										->first();
						$ema = 'ema_'.$upper;
						$driving_price = Config::get('services.algorithm.driving_price');
						$candle_size = (($hp-$lp)<>0)? ($hp-$lp) : 0.00000001;
						$narrowing = (isset($this->deviation_band[$comparison_time])&&isset($this->deviation_band[$t]))? ($this->deviation_band[$comparison_time] > ($this->deviation_band[$t] * 1.25 )) : false;		// CRITICAL REFACTOR: This 1.25 multiplier is just a test.
						if($slope=='bull' && $last->action==='sell' && isset($this->$ema[$t]) && $this->$ema[$t] < $action_price && !$continuation_price){
							// In a breakaway pattern, the candle defies pattern explanation by doing something like dropping hard after a support level.  The candle must hold strong
							$cth_pct = abs(($hp-$action_price)/$candle_size);	// Take the difference between high and close and divide by candle height to see if we are high or low
							if(false!==self::isBreakaway($t,$action_price,$increment,$slope) && $cth_pct <= $driving_price){
								$reason[] = 'Breakaway';
								$flag = true;
								$broken = true;
							}
							// If the last candle was not a level at all OR is a supporting level
							if((false===self::isLevel($t) && false===self::isLevel(($t-$increment)) )||false!==self::isLevel(($t-$increment),$slope)){
								// When we check for "isLevel" we always check for the opposite (ie. we pass 'bear' to confirm if the level resulted at the end of a 'bull' run which results in a sell)
								if(false !== $lvl){
									if($comparison_price <= (($lp+$ap)/2)){
										$flag = true;
									}
								} else if(false !== ($lvl = self::isLevel((($last->time)-$increment),$opposite))){
									if($comparison_price <= (($lp+$ap)/2)){
										$flag = true;
									}
								} else {
									// We need to check to see if there's been a reversal (sell level) between this and the last buy
									// If there is a level between this and the sell which supports this action
									// If we're currently bullish, we want to look for the most recent bear slope as that would indicate a buy action
									$sandwich = self::getPrecedingLevel($t);
									if(false===$sandwich){
										if($comparison_price <= (($lp+$ap)/2)){
											$flag = true;
										}
									} else if(($sandwich['time'] > $last->time) && ($sandwich['slope']==$opposite)){
										$flag = true;
									}
								}
							}
							if($flag==true){
								$act = 'buy';
								// We do not wait if this is a verified breaker
								if(($broken!=true && (!isset($scenario) || $scenario == 0) ) || $narrowing){
									 $status = 'wait';
								} else if($broken == true){
									$order_type = 'market';
								}
								$pattern[] = $reason[] = 'Break-up';
							}
						} else if($slope=='bear' && $last->action==='buy' && isset($this->$ema[$t]) && $action_price<($this->$ema[$t]) && !$continuation_price){
							$cth_pct = abs(($action_price-$lp)/$candle_size);	// Take the difference between high and close and divide by candle height to see if we are high or low
							if(false!==self::isBreakaway($t,$action_price,$increment,$slope) && $cth_pct <= $driving_price){
								$reason[] = 'Breakaway';
								$flag = true;
								$broken = true;
							}
							if((false===self::isLevel($t) && false===self::isLevel(($t-$increment)) )||false!==self::isLevel(($t-$increment),$slope)){
								if(false !== $lvl){
									if($comparison_price >= (($hp+$ap)/2)){
										$flag = true;
									}
								} else if(false !== ($lvl = self::isLevel((($last->time)-$increment),$opposite))){
									if($comparison_price >= (($hp+$ap)/2)){
										$flag = true;
									}
								} else {
									$sandwich = self::getPrecedingLevel($t);
									if(false===$sandwich){
										if($comparison_price >= (($hp+$ap)/2)){
											$flag = true;
										}
									} else if(($sandwich['time'] > $last->time) && ($sandwich['slope']==$opposite)){
										$flag = true;
									}
								}
							}
							if($flag==true){
								$act = 'sell';
								if(($broken!=true && (!isset($scenario) || $scenario == 0) ) || $narrowing){
									$status = 'wait';
								} else if($broken == true){
									$order_type = 'market';
								}
								$pattern[] = $reason[] = 'Break-down';
							}
						}
					}
				}
				
				// DEFERRED ACTIONS
				// If the last action was a "wait", take action on the one which preceded it unless a different pattern has already formed here
				// We only take action when a level has formed above (sell) or below (buy) the previous level
				// We also ignore this if the next level legitimizes the last action (ie. if I bought and the price is now higher than where I bought)
				if((!empty($last)&&!empty($last->status)&&$last->status=='wait')||(!empty($action)&&!empty($action->status)&&$action->status=='wait')){
						$compare_event = (!empty($action)&&!empty($action->status)&&$action->status=='wait')? $action : $last;
						$original = Action::orderBy('time','DESC')
									->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
									->where('time','<',$compare_event->time)
									->where('status','=','done')
									->first();
						$price_compare = false;
						$proceed = true;
						if(!empty($original)){
							// If this is a break-through pattern, we need to confirm the pricing as long as it is not a wait.
							// If it's a standard wait, we just need to confirm the slope has remained the same.
							if((false !== strpos(strtolower($compare_event->reason),'break')) || (false !== strpos(strtolower($compare_event->reason),'catch'))){
								// Make sure that this is not a first wait check.
								if($status=='wait'){
									if($compare_event->status!='wait'){
										$proceed = false;
									}
								// Else (not first check)
								} else {
									if($compare_event->action=="sell"){
										if(false !== strpos(strtolower($compare_event->reason),'break')){
											if(false!==($prlvl = self::getPrecedingLevel($t,$opposite)) && $prlvl['time'] > $last->time){
												$proceed = true;
											} else if(false!==self::enteredLevel($action_price,$t)){
												$proceed = true;
											} else {
												$broken_past = (false !== strpos(strtolower($compare_event->reason),'kicker') || false !== strpos(strtolower($compare_event->reason),'checkmark'))? true : false;
												// On a breakthrough containing another reason, proceed if the price is lower than the original action
												$proceed = ($broken_past) ? ($original->price > $action_price) : ($original->price < $action_price);
											}
										} else {
											$proceed = ($compare_event->price > $action_price);
										}
									} else if($compare_event->action=="buy"){
										if(false !== strpos(strtolower($compare_event->reason),'break')){
											if(false!==($prlvl = self::getPrecedingLevel($t,$opposite)) && $prlvl['time'] > $last->time){
												$proceed = true;
											} else if(false!==self::enteredLevel($action_price,$t)){
												$proceed = true;
											} else {
												$broken_past = (false !== strpos(strtolower($compare_event->reason),'kicker') || false !== strpos(strtolower($compare_event->reason),'checkmark'))? true : false;
												// On a break up, we want to buy (original action would be a sell). The original price should be higher than the new price (buy the dip after the break)
												$proceed = ($broken_past) ? ($original->price < $action_price) : ($original->price > $action_price);
											}
										} else {
											$proceed = ($compare_event->price < $action_price);
										}
										// On a spike and reversal, we sell a high, so on a catch we want to buy where it returns if it returns bullish (higher than buy order which preceded the spike sale).
										// CRITICAL REFACTOR: Why are we using original actions instead of levels for catches?
									}
									// Increments are 300, 900, etc seconds but recurrence is 120 seconds, so take the floor of increment/2 to wait for a different candle
									if(false === strpos(strtolower($compare_event->reason),'catch') && isset($compare_event->waits) && $compare_event->waits >= max(10,floor($increment/2/60))){
										$proceed = true;
										//  CRITICAL REFACTOR: Make wait counts a setting... This should be treated differently than a standard wait.
									}
									if(!empty($last) && $last->status=='wait' && !empty($action) && $action->status=='wait'){
										if($action->action=='buy' && $action_price > $last->price && $action_price > $original->price && $action->waits > 0){
											$proceed = true;
										}
										if($action->action=='sell' && $action_price < $last->price && $action_price < $original->price && $action->waits > 0){
											$proceed = true;
										}
									}
								}
							} else {
								// Make sure that this is not a first check.
								if($status=='wait'){
									if($compare_event->status!='wait'){
										$proceed = false;
									}
								} else {
									if(isset($compare_event->waits) && $compare_event->waits >= max(5,floor($increment/2/60))){
									//	$proceed = true;
									//  CRITICAL REFACTOR: Make wait counts a setting... Not sure we wanna do this, though
									}
								}
							}
							if(isset($scenario) && $scenario > 0){
								$proceed = true;
							}
							if($proceed !== false){
								$price_compare = ($compare_event->action=="sell")? (($slope=='bear')? true : false) : (($slope=='bull')? true : false);
							}
							if($compare_event->action=="buy"&&$original->action=="sell"&&$price_compare&&(false==($lvl = self::isLevel($t))||(false!==$lvl  && ($lvl['type'] == 'continued' && $lvl['slope']=$slope) || ($lvl['type'] != 'continued' && $lvl['slope']==$opposite)))){
								$act = 'buy';
								$status = null;
								$order_type = 'market';
								$pattern[] = $reason[] = 'Deferred '.$compare_event->reason;
							} else if(isset($compare_event->waits)){
								$compare_event->waits = ($compare_event->waits)+1;
								$compare_event->save();
							}
							if($compare_event->action=="sell"&&$original->action=="buy"&&$price_compare&&(false==($lvl = self::isLevel($t))||(false!==$lvl  && ($lvl['type'] == 'continued' && $lvl['slope']==$slope) || ($lvl['type'] != 'continued' && $lvl['slope']==$opposite)))){
								$act = 'sell';
								$status = null;
								$order_type = 'market';
								$pattern[] = $reason[] = 'Deferred '.$compare_event->reason;
							} else if(isset($compare_event->waits)){
								$compare_event->waits = ($compare_event->waits)+1;
								$compare_event->save();
							}
						}
				}
			}
			if(!empty($pattern)){
				// REFACTOR: See 2nd and 3rd lines below.  The idea of open orders is slighly concerning.
				// NOTE: If this action is different and the last action was done (or null because it is building data for first time), take it
				//		For different actions, the last action will only get one chance to re-run anyway and if it succeeds, likely this action is bad
				//		If this action is the same and the last action not done or null (open or wait), take action (last one will probably cancel if open; waits never execute)
				//		This should re-run the same action as long as the order hasn't completed.
				$action_condition = (!empty($action) && ($action->status=='wait' || $action->status=='cancelled' ) && (empty($status) || $status!=='wait'))? true : false;
				if($action_condition){
					 if(isset($last) && $last->status=='done' && $last->action==$act){
						$action_condition = false; 
					 }
				}
				$last_condition = (!empty($last) && ($last->action)!==$act && ((Config::get('app.env')!=='live'&&Config::get('app.env')!=='production')||(!empty($last->status)&&in_array($last->status,['done']))))? true : false;
				if($action_condition||empty($last)||$last_condition||(($last->action)===$act && (empty($last->status) || !in_array($last->status,['done'])))){
					if(Config::get('app.env')!=='live'&&Config::get('app.env')!=='production'){
						$status = "done";
					}
					// If we just cancelled an order after approving it multiple times, we need to wait on it at least once.
					if(!empty($action) && $action->status=='cancelled'){
						$status = 'wait';
					}
					// We COULD use the project price function but at this short a timeframe it's useless
					// Order actions repeat ea 60 secs & actions ea 2 mins, so the max a candle would have built for at this point would be 3 minutes max delay would be 1 minute
					// REFACTOR: Thse values (3 and 60) should be stored as env vars and used in the schedule kernel
					$duration = $increment/2/60;
					$range_per_minute = abs($op-$cp)/$duration;
					$action_price = ($act==='buy')? ($action_price + $range_per_minute) : ($action_price - $range_per_minute);	// This basically gives us the max/min expected range for 1 min time.
					if(empty($action)){
						$action = new Action();
						$action->projected_price = $action_price;
						$action->gradient_projection = !empty($proj)? $proj : 0;
					}
					$action->sign = $pid;
					$action->time = $t;
					$action->increment = $increment;
					$action->bands = $lower.'-'.$upper;
					$action->price = $action_price;
					$action->reason = trim(implode(', ',$reason));
					$action->status = (Config::get('services.algorithm.waits')!=false)? $status : null;
					if(!empty($action->status) && ($action->status)=='wait'){
						if(isset($action->waits)&&empty($action->waits)){
							$action->waits = 1;
							// We do not have to iterate this for deferred actions (re-waits) because that is already handled above.
						}
					}
					$action->scenario = !empty($scenario)? $scenario : 0;
					$action->variance = !empty($variance)? $variance : 0;
					$action->gradient = !empty($g)? $g : 0;
					$action->deviation = !empty($this->deviation_band[$t])? $this->deviation_band[$t] : 0;
					if(!empty($order_type)){
						$action->type = $order_type;
					}
					if(!empty($limit_method)){
						$action->limit_method = $limit_method;
					}
					// The previous order was "done" but did not fully fill before being cancelled, we set a size to match the last fill
					// Sometimes there is a very slight difference between a size and a fill based primarily on price movement or fee amount, so it could be that an order was completed but
					// the size just changed between here and the order action.  The orderProcessor is set up to address this but just in case, we consider 100%-fee to equal 100%
					if(!empty($last) && isset($last->size) && isset($last->filled) && !empty($last->filled) && ($last->filled > 0.0) && ($last->size - (($last->size) * (Config::get('services.algorithm.limit_variance')))) > ($last->filled)){
						if(isset($this->base_min_size) && $last->filled < $this->base_min_size){
							$action->size = $this->base_min_size;
						} else {
							$action->size = $last->filled;
						}
					}
					$action->action = $act;
					if($this->save_results!==false){
						$action->save();
						// Since we have a new order in place, past actions are cancelled
						// Since the above logic only stores a new action if the last status is done, this is safe (the action would be the same as this).
						if(!empty($last)){
							Action::where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])
									->where('time','>=',$last->time)
									->where('time','<',$t)
									->where(function ($q){
										$q->orWhere('status','=','wait')
										->orWhereNull('status');
									})->update(['status'=>'cancelled','limit_method'=>'overridden']);
						}
					}
					return $act;
				}
			}
		return false;
	}

	public function getEMA($period=12,$start=null,$increment=null,$type='close'){
		$history = self::getHistory($start,0,$increment);
		switch($type){	// 0:time,1:low,2:high,3:open,4:close,5:volume
			case 'low':
				$type = 1;
				break;
			case 'high':
				$type = 2;
				break;
			case 'open':
				$type = 3;
				break;
			case 'close':
			default:
				$type = 4;
				break;
		}
		self::getSMA($period,$start,$increment,$type);
		if(!empty($history)){
			$first_time = $history[0][0];
			$history = array_reverse($history);
			$ema = 'ema_'.$period;
			$sma = 'sma_'.$period;
			$this->$ema = [];
			// history now starts with the newest, so we're starting at the end, which is the oldest
			$emaobjs = Ema::where(['increment'=>$increment,'band'=>$period,'sign'=>$this->product])->where('time','>=',$first_time)->get();
			for($i=(count($history)-$period);$i>0;$i--){
				$emaobj = null;
				if(!empty($emaobjs)){
					if($emaobjs->contains('time',$history[$i][0])){
						$emaobj = $emaobjs->where('time',$history[$i][0])->first();
					}
				}
				if(empty($emaobj) || $i<=1){
					if(empty($emaobj)){
						$emaobj = new Ema();
					}
					$emaobj->sign = $this->product;
					$emaobj->time = $history[$i][0];
					$emaobj->increment = $increment;
					$emaobj->band = $period;

					if($i==(count($history)-$period)&&!empty($this->$sma[$history[$i-1][0]])){
						$emaobj->ema = $this->$ema[$history[$i-1][0]] = $this->$sma[$history[$i-1][0]];
					} else if(!empty($this->$ema[$history[$i][0]])){
						$c = $history[$i-1][$type];			// Closing Price *today*
						$p = $this->$ema[$history[$i][0]];	// EMA *yesterday*
						$k = ($this->smoothing/($period+1));// Smoothing variable
						$calc = (($c*$k)+($p*(1-$k)));
						$emaobj->ema = $this->$ema[$history[$i-1][0]] = $calc;
					}
					if($this->save_results!==false){
						$emaobj->save();
					}
				}
				$this->$ema[$history[$i-1][0]] = $emaobj->ema;
			}
			ksort($this->$ema);
			return $this->$ema;
		}
		return false;
	}
	
	public function getBreaks($period=5,$start=null,$increment=null){
		$history = self::getHistory($start,0,$increment);
		$breaks = [];
		if(empty($history)){
			return [];
		}
		for($i=0;$i<count($history);$i++){
			if(isset($history[$i+1])&&(($history[$i][0])-($history[$i+1][0])) > $increment){
				$breaks[$history[$i][0]] = ['start'=>($history[$i][0])+1,'end'=>($history[$i+1][0])-1];
			}
		}
		return $breaks;
	}

	/**		
		The increment isn't really used here, it's just passed to the history
	**/
	public function getSMA($period=5,$start=null,$increment=null,$type='close'){
		$history = self::getHistory($start,0,$increment);
		switch($type){	// 0:time,1:low,2:high,3:open,4:close,5:volume
			case 'low':
				$type = 1;
				break;
			case 'high':
				$type = 2;
				break;
			case 'open':
				$type = 3;
				break;
			case 'close':
			default:
				$type = 4;
				break;
		}
		if(false!==$history){
			$sma = 'sma_'.$period;
			$this->$sma = [];
			$latest = Sma::orderBy('time','DESC')->where(['increment'=>$increment,'period'=>$period,'sign'=>$this->product])->first();
			if(empty($start)){
				$start = $history[0][0];
			}
			$history = array_reverse($history);	// This makes it go newest to oldest
			$first_index = false;
			if(!empty($latest)){
				$first_index = array_search($latest->time, array_column($history,0));
			}
			for($i=0;$i<count($history);$i++){
				if($first_index!==false&&$i>$first_index){
					break;
				}
				if($this->save_results!==false){
					$collection = [];
					$sm = Sma::where(['increment'=>$increment,'period'=>$period,'sign'=>$this->product])->where('time',$history[$i][0])->first();
					if(empty($sm)||$i==$first_index){
						if(empty($sm)){
							$sm = new Sma();
						}
						$sm->sign = $this->product;
						$sm->period = $period;
						$sm->time = $history[$i][0];
						$sm->increment = $increment;
						for($y=$i;$y<($period+$i);$y++){
							if(!isset($history[$y])){
								break 2;
							}
							$collection[] = $history[$y][$type];
						}
						$sum = array_sum($collection);
						$count = count($collection);
						$sm->price = $sum/$count;
						$standard_deviation = self::getStandardDeviation($collection);
						$sm->bollinger_upper = ($sum/$count) + ($standard_deviation*2);
						$sm->bollinger_lower = ($sum/$count) - ($standard_deviation*2);
						$sm->deviation = $standard_deviation;
						$sm->save();
					}
				}
			}
			$query = Sma::orderBy('time','ASC')->where(['increment'=>$increment,'period'=>$period,'sign'=>$this->product])->where('time','>=',$start);
			$this->$sma = $query->pluck('price','time');
			$bollinger_setting = Config::get('services.algorithm.bollinger_period');
			$deviation_setting = Config::get('services.algorithm.deviation_period');
			if($bollinger_setting!=='long'&&!empty($this->bollinger_upper)&&!empty($this->bollinger_lower)){
				// Do nothing, we'll use the first set bollinger bands from the short term
			} else {
				$this->bollinger_upper = $query->pluck('bollinger_upper','time');
				$this->bollinger_lower = $query->pluck('bollinger_lower','time');
			}
			if($deviation_setting!=='long'&&!empty($this->deviation_band)){
				// Do nothing
			} else {
				$this->deviation_band = $query->pluck('deviation','time');
			}
			return $this->$sma;
		}
		return false;
	}

	/**
		Not intended for real-time data.  Trade and book endpoints for that.
		This should be good for determining SMA?
		Increment is minimum granularity in seconds.

		1,0,60 (1 day with minute to minute results)
		7,0,3600 (1 week with hourly results)

		Time, Low, High, Open, Close, Volume
		
		Limit is 300 on Coinbase
		REFACTOR: Limit could be different on Alpaca
	**/
	public function getHistory($start=0,$end=0,&$increment=300,$timezone='America/New_York'){
		// If passing a period length, interval is not editable, we'll take whatever is the most granular
		// The period can be changed on the UI with Zoom
		if(empty($this->product)){
			return false;
		}
		// Max seconds backwards
		$seconds = 300*$increment;
		if(!empty($start)){
			$st_temp = time()-$seconds;
			if($st_temp>$start){
				$start = null;
			}
		}
		if(!empty($this->history[$this->product.'-'.$increment])){
			if(!empty($start)){
				// Return only the data set beginning where time equals $start minus $increment (always give at least 1 backwards)
				$key = array_search(($start-$increment), array_column($this->history[$this->product.'-'.$increment],0));
				$length = null;
				if($end!==0){
					$length = (($end-$start)/$increment);
				}
				return array_slice($this->history[$this->product.'-'.$increment],$key,$length);
				// CRITICAL REFACTOR: We must be sure that history is always empty whenever it's being called.
			}
			if($end!==0){
				// Since start is 0, start is actually 300*$increment
				$now = time();
				$length = floor(($end-($now-(300*$increment)))/$increment);
				return array_slice($this->history[$this->product.'-'.$increment],0,$length);
			}
			return $this->history[$this->product.'-'.$increment];	
			// CRITICAL REFACTOR: We must be sure that history is always empty whenever it's being called.
		}
		if(!empty($start)){
			$start = new DateTime("@$start");
		} else {
			if(empty($increment)){
				$increment = 60*5; // 5 min chart default
			}
			// Hours for all units
			$hrs = $seconds/60/60;
			$days = $hrs/24;
			$hrs = floor($hrs);
			$days = floor($days);
			if($increment==60){
				$start = new DateTime('-'.$hrs.' hours');
			} else {
				$start = new DateTime('-'.$days.' days');
			}
		}
		// CRITICAL REFACTOR: $this->history ignores "end" date.
		if($end==0){
			$end = new DateTime();
		} else {
			$end = new DateTime('-'.$end.' seconds');
		}
		$start->setTimezone(new DateTimeZone($timezone));
		$end->setTimezone(new DateTimeZone($timezone));
		$start = $start->format('Y-m-d\TH:i:sO');
        $end = $end->format('Y-m-d\TH:i:sO');
        $api = new CoinApiController();
		$this->history[$this->product.'-'.$increment] = $api->run('products/'.$this->product.'/candles','GET',['start'=>$start,'stop'=>$end,'granularity'=>$increment]);
		if(empty($this->history[$this->product.'-'.$increment])){
			return false;
		}
		 $this->history[$this->product.'-'.$increment] = array_reverse($this->history[$this->product.'-'.$increment]);
		 return $this->history[$this->product.'-'.$increment];
	}
}

?>
