<?php
namespace App\Http\Controllers;
use App\Http\Controllers\CoinApiController;
use App\Http\Controllers\CoinController;
use App\Models\Product as Product;
use Illuminate\Support\Facades\Log as Log;

class CoinExchangeController extends Controller {

	public function __construct(){
		// Do nothing
	}

	/**
		Passing "*" cancels all orders
	**/
	public function getOrder($id){
		$api = new CoinApiController();
		if($id=="*"){
			return $api->run('orders');
		}
		return $api->run('orders/'.$id);
	}

	public function getFills($id){
		$api = new CoinApiController();
		return $api->run('fills?order_id='.$id);
	}

	/**
		getAccountHolds is useful for identifying if an order is still being filled
	**/
	public function getAccountHolds($id){
		$api = new CoinApiController();
		return $api->run('accounts/'.$id.'/holds');
	}

	/**
		shares must be larger than "base_min_size " and smaller than "base_max_size " incremended by "base_increment"
	**/

	public function placeOrder($side='buy',$type='market',$price=null,$size=1,$product_id='BTC-USD',$post_only=false,$funds=false,$stop=false){
		$api = new CoinApiController();
		$data = [
			//'client_oid' => $i // This is a UUID created by own application, which we can get from the database if we want to use it.
			'type' => $type, // 'limit' or 'market', default is limit but we'll set default to market
			'side' => $side, // 'buy' or 'sell'
			'product_id' => $product_id // BTC-USD, BTC-ETH, etc.
		];
		
		// CRITICAL REFACTOR: This doesn't work for "FUNDS"
		$smallest = Product::where('sign',$product_id)->where('type','coin')->value('quote_increment');
		$smallest = rtrim($smallest,'0');
		$decimals = strpos(strrev($smallest), ".");
		if(!empty($decimals)){
			$price = number_format($price,$decimals);
			$price = str_replace(',','',$price);
		}
		$smallest = Product::where('sign',$product_id)->where('type','coin')->value('base_increment');
		$smallest = rtrim($smallest,'0');
		$decimals = strpos(strrev($smallest), ".");
		if(!empty($decimals)){
			$size = number_format($size,$decimals);
		}
		if($type!=='limit'){
			if(false===$funds){
				$data['size'] = $size;
			} else {
			// Also accepts "funds" argument which would use the quote fund amount (ie. 150 funds would be $150 whereas 150 size would be 150 BTC).
				$data['funds'] = $size;
			}
		} else {
			$data['size'] = $size;
		}
		// To set a stop loss
		if(false!==$stop){
			$data['stop_price'] = $price;
			if($side==='buy'){
				$data['stop'] = 'entry';
			} else {
				$data['stop'] = 'loss';
			}
		}
		if($type==='limit'){
			$data['price'] = $price;
			if(!empty($post_only)){
				// $data['post_only'] = true;
				// we keep getting errors on this.
			}
			$data['time_in_force'] = 'GTT';
			$data['cancel_after'] = 'hour';
		}

		$results = $api->run('orders','POST',$data);
		return $results;
	}

	/**
		Passing "*" cancels all orders
	**/
	public function cancelOrder($id){
		Log::debug('Cancelling Order '.$id);
		$api = new CoinApiController();
		if($id=="*"){
			return $api->run('orders','DELETE');
		}
		return $api->run('orders/'.$id,'DELETE');
	}

}

?>
