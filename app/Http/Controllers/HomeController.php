<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\CoinController;
use Illuminate\Support\Facades\Log as Log;
use App\Models\Macd as Macd;
use App\Models\Action as Action;

class HomeController extends Controller
{
	public function searchSigns(Request $request)
	{
		$handle = strtoupper($request->input('search'));
		if(!empty($handle)){
			$product = new CoinController($handle);
			return json_encode($product->getProduct($handle));
		}
		return false;
	}
	
	public static function getMacd(Request $request){
		if($request->has('product')&&$request->has('type')&&$request->has('bands')&&$request->has('increment')){
			$id = $request->input('product');
			$increment = $request->input('increment');
			$bands = explode(',',$request->input('bands'));
			$lower = min($bands);
			$upper = max($bands);
			$latest_macd = Macd::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$id])->first();
			if(!empty($latest_macd)){
				return $latest_macd;
			}
		}
		return false;
	}

	public static function getAction(Request $request){
		if($request->has('product')&&$request->has('type')&&$request->has('emas')&&$request->has('increment')){
			$id = $request->input('product');
			$increment = $request->input('increment');
			$emas = explode(',',$request->input('emas'));
			$lower = min($emas);
			$upper = max($emas);
			$latest_action = Action::orderBy('time','DESC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$id])->first();
			if(!empty($latest_action)){
				return $latest_action;
			}
		}
		return false;
	}

	public static function getTicker(Request $request){
		if($request->has('product')&&$request->has('type')){
			$id = $request->input('product');
			if($request->input('type')==='coin'){
				$product = new CoinController($id);
			} else {
				$product = new StockController($id);
			}
			$ticker = $product->getPrice($id);
			if(!empty($ticker)){
				return $ticker;
			}
		}
		return false;
	}
	
	public static function showOrders(Request $request){
		$start = null;
		// REFACTOR: We really only support 1 product
		if($request->has('product')){
			$pids = explode(',',$request->input('product'));
		}
		if($request->has('smas')){
			$smas = explode(',',$request->input('smas'));
		}
		if($request->has('emas')){
			$emas = explode(',',$request->input('emas'));
		}
		if($request->has('start')){
			$start = $request->input('start');
		} else if($request->has('increment')){
			$increment = $request->input('increment');
		}
		if($request->has('end')){
			$end = $request->input('end');
		}
		foreach($pids as $pid){
			if(false!==strpos($pid,'-')){
				$product = new CoinController($pid);
			} else {
				$product = new StockController($pid);
			}
		}
		$start = !empty($start)? $start : (time() - ($increment * Config::get('services.coinbase.unit_limit')));
		$lower = min($emas);
		$upper = max($emas);
		if(config('app.env')!='production'&&config('app.env')!='live'){
			$orders = Action::orderBy('time','ASC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])->where('time','>=',$start)->get();
		} else {
			$orders = Action::orderBy('time','ASC')->where(['increment'=>$increment,'bands'=>$lower.'-'.$upper,'sign'=>$pid])->whereNotNull('order_id')->where('time','>=',$start)->get();
		}
		$final_price = self::getTicker($request);
		if(isset($product->getHistory($start, 0, $increment)[0])){
			$first_price = $product->getHistory($start, 0, $increment)[0][3];
		} else {
			$first_price = 0;
		}
		return view('partials.history', ['orders'=>$orders,'first_price'=>$first_price,'final_price'=>$final_price])->render();
	}
	
	public function showChart(Request $request){
		$start = $increment = $starter = null;
		$end = 0;
		$pids = ["BTC-USD"];
		$smas = $smas_output = $emas_output = [];
		$emas = ['9', '26'];
		$actions = null;
		if($request->has('product')){
			$pids = explode(',',$request->input('product'));
		}
		if($request->has('smas')){
			$smas = explode(',',$request->input('smas'));
		}
		if($request->has('emas')){
			$emas = explode(',',$request->input('emas'));
		}
		if($request->has('start')){
			$start = $request->input('start');
		} else if($request->has('increment')){
			$increment = $request->input('increment');
		}
		if($request->has('end')){
			$end = $request->input('end');
		}
		$history = $levels = [];
		foreach($pids as $pid){
			if(false!==strpos($pid,'-')){
				$product = new CoinController($pid);
				$history = $product->getHistory($start, $end, $increment);
				$starter = empty($start)&&!empty($history) ? $history[0][0] : $start;
			} else {
				$product = new StockController($pid);
				$history = $product->getHistory($start, $end, $increment);
				$starter = empty($start)&&!empty($history) ? history[0]->t : $start;
			}
			$flag = false;
			foreach(array_unique(array_merge($emas,$smas)) as $period){
				if(in_array($period,$smas)){
					$smas_output[$period] = $product->getSMA($period, $start, $increment);
				}
				if(in_array($period,$emas)){
					$emas_output[$period] = $product->getEMA($period, $start, $increment);
				}
			}
			$product->getMACD($emas, $start, $increment);
			// Levels and Actions should be run after macd since crossovers and bollinger bands come into play there.
			$levels = $product->getHistoricLevels($increment, $pid);
			// Action should occcur after level because actions also check for level crossovers
			$actions = $product->getActions($emas, $start, $increment);
			$bollinger = [
							'Upper'=>$product->bollinger_upper,
							'Lower'=>$product->bollinger_lower,
						];
			$breaks = $product->getBreaks($period,$start,$increment);
		}
		return view('partials.chart', compact(
			'start',
			'history',
			'levels',
            'emas_output',
            'smas_output',
            'actions',
			'bollinger',
			'starter',
			'breaks'
			))->render();
	}	
	
    public function index(Request $request)
    {
		$start = $increment = $starter = null;
		$end = 0;
		$pids = ["BTC-USD"];
		$smas = $smas_output = $emas_output = [];
		$emas = ['9', '26'];
		$actions = null;
		if($request->has('product')){
			$pids = explode(',',$request->input('product'));
		}
		if($request->has('smas')){
			$smas = explode(',',$request->input('smas'));
		}
		if($request->has('emas')){
			$emas = explode(',',$request->input('emas'));
		}
		if($request->has('start')){
			$start = $request->input('start');
		} else if($request->has('increment')){
			$increment = $request->input('increment');
		}
		if($request->has('end')){
			$end = $request->input('end');
		}
		$history = $levels = [];
		foreach($pids as $pid){
			if(false!==strpos($pid,'-')){
				$product = new CoinController($pid);
				$history = $product->getHistory($start, $end, $increment);
				if(empty($history)){
					return view('pages.home', ['error'=>'no data']);
				}
				$starter = empty($start)&&!empty($history) ? $history[0][0] : $start;
			} else {
				$product = new StockController($pid);
				$history = $product->getHistory($start, $end, $increment);
				if(empty($history)){
					return view('pages.home', ['error'=>'no data']);
				}
				$starter = empty($start)&&!empty($history) ? history[0]->t : $start;
			}
			foreach(array_unique(array_merge($emas,$smas)) as $period){
				if(in_array($period,$smas)){
					$smas_output[$period] = $product->getSMA($period, $start, $increment);
				}
			}
			$breaks = $product->getBreaks($period,$start,$increment);
		}
		if(!empty($product)){
			$all = $product->getProduct('*');
		}
		// REFACTOR: Need a "favorites" table AND need to return them with coins and stocks
		// REFACTOR: We only really need the "all" variable... all others serve the chart
		$favorites = ['coin'=>$all];
        return view('pages.home', compact(
			'start',		// Built above
			'history',		// Built above
			'favorites',	// Built above
            'smas_output',	// Built during getSMA
			'starter',		// Built above
			'breaks'		// Built during getSMA
			));
    }
}
