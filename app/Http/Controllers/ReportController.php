<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as Log;
use App\Models\Action as Action;
use App\Models\Product as Product;

class ReportController extends Controller
{
    public function getReports(Request $request)
    {
		$reason = $increment = $end = $action = $pids = null;
		$start = (time()-(30 * 24 * 60 * 60));
		if($request->has('product')){
			$pids = explode(',',$request->input('product'));
		}
		if($request->has('emas')){
			$emas = $request->input('emas');
		}
		if($request->has('start')){
			$start = $request->input('start');
		}
		if($request->has('increment')){
			$increment = $request->input('increment');
		}
		if($request->has('reason')){
			$reason = $request->input('reason');
		}
		if($request->has('end')){
			$end = $request->input('end');
			$end = strtotime($end);
		}
		$query = Action::where('time','>=',$start);
		if(!empty($pids)){
			$query->where(function ($q) use ($pids) {
				$q->where(function($r){
					foreach($pids as $pid){
						$r->orWhere('sign',$pid);
					}
				});
			});
		}
		if(!empty($end)){
			$query->where('time','=<',$end);
		}
		if(!empty($emas)){
			$query->where('bands',$emas);
		}
		if(!empty($increment)){
			$query->where('bands',$increment);
		}
		if(!empty($reason)){
			$query->where('reason','LIKE','%'.$reason.'%');
		}
		$orders = $query->orderBy('sign','DESC')->orderBy('time','DESC')->get();
		$products = Product::all()->pluck('sign');
		$options = [];
		$options['emas'] = [9,12,26];
		$options['increments'] = [300,900,3600];
		$options['actions'] = ['buy','sell'];
		$reasons = [
			'Break',
			'Catch',
			'CheckMark',
			'Current Crossing',
			'Deferred',
			'Double',
			'EMA Gradient',
			'Head and Shoulders',
			'Kicker',
			'Previous',
			'Sideways Trailing',
			'Stepping Reversal',
			'Triple',
			'Spike',
		];
        return view('pages.reports', compact(
			'start',
			'end',
			'products',
			'options',
			'reasons',
			'orders',
		));
    }
}
