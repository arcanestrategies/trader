<?php
namespace App\Http\Controllers;
use App\Models\Macd as Macd;
use App\Models\Ema as Ema;
use App\Models\Sma as Sma;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller {

	public function __construct($id=null){
		//
	}

	public function getStandardDeviation(array $collection){
		if(empty($collection)){
			return 0;
		}
		// 1. Get the mean of all numbers in the array.
		$mean = array_sum($collection)/count($collection);
		// 2. Loop through all numbers and subtract the mean.  Then square the result.
		for($i=0;$i<count($collection);$i++){
			$temper = $collection[$i]-$mean;
			$collection[$i] = $temper*$temper;
		}
		// 3. With the data from step 2, get the mean of all numbers.
		$mean = array_sum($collection)/count($collection);
		// 4. Now take the square root of 3.
		return sqrt($mean);
	}


	public function getDivergence($product,$start,$end){
		$all_divergence = DB::table('emas')
								->select(DB::raw('MAX(ema)-MIN(ema) as margin')) 
								->groupBy('time','sign')
								->where('time','>',$start)
								->where('time','<',$end)
								->where('sign',$product)
								->get();
		$max = $all_divergence->max('margin');
		$avg = $all_divergence->avg('margin');
		return ['max'=>$max,'average'=>$avg];
	}

	/**
		REFACTOR:  The problem with this approach is two-fold:
		
		(1) It calculates all the averages in advance, so it isn't actually a true trailing indicator of history, only of present, which is fine for our purposes.
		(2) CRITICAL: More importantly, it doesn't take into account the bands and intervals (ie.12-26 on 5 or 15 min chart)
	**/
	public function getGlobalMargin($product,$start){
		$highest_of_all = [];
		$start = strtotime($start);
		foreach(['signal_action_margin','signal_signal_margin','action_action_margin'] as $s){
			$highest = 'highest_'.$s;
			$average = 'average_'.$s;
			if(!empty(Macd::where('sign',$product)->where('time','>',$start)->get())){
				$this->$highest = $highest_of_all[$s] = Macd::where('sign',$product)->where('time','>',$start)->max($s);
				$this->$average = Macd::where('sign',$product)->where('time','>',$start)->avg($s);
			} else {
				$this->$highest = $highest_of_all[$s] = null;
				$this->$average = null;
			}
		}
		$all_divergence = DB::table('emas')
								->select(DB::raw('MAX(ema)-MIN(ema) as margin')) 
								->groupBy('time','sign')
								->where('time','>',$start)
								->where('sign',$product)
								->get();
		$this->highest_divergence_margin = $all_divergence->max('margin');
		$this->average_divergence_margin = $all_divergence->avg('margin');
		//$this->divergence_threshold = $this->average_divergence_margin*$this->divergence_coefficient;
		$this->all_time_margin = max($highest_of_all);
	}

}

?>
