<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Config;

// REFACTOR this is specifically for coinbase, so this should be renamed everywhere

class StockApiController extends Controller {
	/**
		200 Success
		400 Bad Request
		401 Unauthorized
		403 Forbidden
		429 Too Many Requests // 3 Requests Per second public, 5 private, 500 total orders per account may remain open
		404 Not Found
		500 Server Error
	**/
	public $limit = 200;	// Per minute
	public $endpoint = null;
	public static $LAST_STATUS = 0;

	public function __construct(){
		// DO NOTHING
	}

	public function run($path,$method='GET',$data=null){
			$session = curl_init();
			$time = time();
			if(false!==strpos(strtolower($path),'http')){
				$url = $path;
			} else {
				$url = Config::get('services.alpaca.endpoint').'/'.$path;
			}
			$headers = [
							//'Content-Type: application/json',
							//'User-Agent: telnet',
							'APCA-API-KEY-ID: '.Config::get('services.alpaca.key'),
							'APCA-API-SECRET-KEY: '.Config::get('services.alpaca.secret'),
						];

			switch ($method)
			{
				case "PATCH":
				case "PUT":
				case "POST":
					curl_setopt($session, CURLOPT_POST, 1);
					if (!empty($data)){
						$data = json_encode($data);
						curl_setopt($session, CURLOPT_POSTFIELDS, $data);
					}
					break;
				case "DELETE":
					curl_setopt($session, CURLOPT_CUSTOMREQUEST, "DELETE");
					break;
				case "GET":
				default:
					if (!empty($data)){
						$get = http_build_query($data);
						$url .= '?'.$get;
					}
					curl_setopt($session, CURLOPT_HTTPGET, 1);
					break;
			}

			curl_setopt($session, CURLOPT_URL, $url);
			curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($session, CURLOPT_SSL_VERIFYPEER,false);
			$response = curl_exec($session);

			self::$LAST_STATUS = curl_getinfo($session, CURLINFO_HTTP_CODE);
			curl_close($session);
			$response = json_decode($response);
			if(self::$LAST_STATUS!==200){
				return false;
			}
			return $response;
	}
}

?>
