<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use App\Http\Controllers\StockApiController;
use App\Http\Controllers\TradeInterface;
use App\Models\Macd as Macd;
use App\Models\Ema as Ema;
use App\Models\Sma as Sma;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\DB;

class StockController extends StatisticsController implements TradeInterface {

	public $base_currency;
	public $quote_currency;
	public $base_min_size;
	public $base_max_size;
	public $quote_increment;
	public $base_increment;
	public $min_market_funds;
	public $product;
	public $agg_limit = 300;
	public $smoothing = 2;
	public $profit = 0.03;	// We want 3%
	public $fee = 0.005;	// We pay 0.5% per transaction (sell + buy = 1%)
	public $signals;
	public $actions;
	public $divergence;
	public $divergence_threshold = 0;
	public $divergence_coefficient = 0.0033;
	public $bollinger_upper = [];
	public $bollinger_lower = [];
	public $band_margin = [];
	public $deviation_band = [];
	public $breaks = [];

	public function __construct($id=null){
		if(null!==$id){
			$this->product = $id;
			$obj = self::getProduct();
			if(!empty($obj)){
				foreach($obj as $p => $v){
					$this->$p = $v;
				}
			} else {
				$this->product = null;
			}
		}
	}	

	public static function getPrice($id){
		$product = new StockController($id);
		$stats = $product->getTicker();
		if(!empty($stats)){
			return $stats->last->bidprice;	// REFACTOR: The ask price is more relevant for sales.
		}
		return false;
	}

	/**
		Pass "*" to get all
	**/
	public function getProduct($id=null){
		$api = new StockApiController();
		if(!empty($id)){
			if($id=="*"){
				return $api->run('v2/assets');
			} else {
				return $api->run('v2/assets/'.$id);
			}
		}
		return $api->run('v2/assets/'.$this->product);
	}
	
	/**
		This provides real-time data but they want us to use the websocket instead... this is "discouraged"
	**/
	public function getTicker(){
        $api = new CoinApiController();
		return $api->run('v1/last_quote/stocks/'.$this->product);
	}

	/**
		Real-time Data
		Limit is 1000.  Default is 100.
	**/
	public function getTrades(){
        $api = new CoinApiController();
		return $api->run('v1/last/stocks/'.$this->product);
	}
	
	public function getHistory($start=null,$end=0,&$increment=300,$timezone='America/New_York'){
		if(!empty($start)){
			if($start<1){
				$start = floor(24*$start);
				$increment='1Min';
				$start = new DateTime('-'.$start.' hours');
			} else {
				if($start===1){
					$increment = ($increment>300)? $increment : 300;	// 5 Minutes is good for 1 full day
				} else if($start>1&&3>=$start){
					$increment = ($increment>900)? $increment : 900;	// 15 Minutes is good for 3 full days
				} else if($start>3&&12>=$start){
					$increment = ($increment>3600)? $increment : 3600;	// 1 hour is good for 12 days
				} else if($start>12&&72>=$start){
					$increment = ($increment>21600)? $increment : 21600;	// 6 hour increments is good for 72 days
				}
				$start = new DateTime('-'.$start.' days');
			}
		} else {
			if(empty($increment)){
				$increment = 60*5; // 5 min chart default
			}
			// Total units
			$units = 300*$increment;
			// Hours for all units
			$hrs = $units/60/60;
			$days = $hrs/24;
			$hrs = floor($hrs);
			$days = floor($days);
			if($increment==60){
				$start = new DateTime('-'.$hrs.' hours');
			} else {
				$start = new DateTime('-'.$days.' days');
			}
		}
		if($end<1){
			$end = floor(24*$end);
			$end = new DateTime('-'.$end.' hours');
		} else {
			$end = new DateTime('-'.$end.' days');
		}
		$start->setTimezone(new DateTimeZone($timezone));
		$end->setTimezone(new DateTimeZone($timezone));
		$start = $start->format('Y-m-d\TH:i:sO');
        $end = $end->format('Y-m-d\TH:i:sO');
        $api = new StockApiController();
		self::getGlobalMargin($this->product,$start);
		switch($increment){
			case $increment > 900:
				$interval = 'day';
				break;
			case $increment > 300:
				$interval = '15Min';
				break;
			case $increment > 60:
				$interval = '5Min';
				break;
			default:
				$interval = '1Min';
				break;
		}

		$data = [
					'symbols'=>$this->product,
					'limit'=>300,
					'start'=>$start,
					'end'=>$end
				];
		if(!empty($this->history[strtotime($start).'-'.$increment])){
			return $this->history[strtotime($start).'-'.$increment];	
			// CRITICAL REFACTOR: We must be sure that history is always empty whenever it's being called.
		}
		try {
			$results = $api->run(Config::get('services.alpaca.data_endpoint').'/v1/bars/'.$interval,'GET',$data);
			$product = $this->product;
			$this->history[strtotime($start).'-'.$increment] = array_reverse($results->$product);
			return $this->history[strtotime($start).'-'.$increment];
		} catch (\Exception $e){
			return false;
		}
		
	}
	
	/**
		Proceeds from the furthest back point moving forward to determine patterns.
		(1) Set a counter for 1, 2 slope vars (last_slope, new_slope) for null, and an empty array (to become associateive), loop over history.
		(2) If current closing price is higher than previous one plus the degree of variability, set the current slope is temporarily bullish.  If lower than previous price temporarily bearish.
		(3) Proceed forward and add 1 to the counter
		(4) If the slope changes direction and last_slope was not null:
		(4.a) If the counter is greater than counter threshold (ENV 3?), store the opening price as level 1.  Store the last slope alongside it: (ie. ['time'=>['slope'=>Z,'level1'=>Y,'level2'=>X]]
			(4.a.1) if the trend is now bullish, store the previous time's low as level 2
			(4.a.2) if the trend is now bearish, store the previous time's high as level 2.
		(4.b) If the current slope is the same as the last level's slope
			(4.b.1) if the current slope is bullish, overwrite the last slope level 2 with the current high.
			(4.b.2) if the current slope is bearish, overwrite the last slope level 2 with the current low.
		(4.c) Set the counter to 1;
	**/
	public function getHistoricLevels($increment,$sign,$start=null){
		// REFACTOR: History API can accept a start date BUT the getHistory function actually takes a range (ie. X days back)
		$history = self::getHistory(null,0,$increment);
		$history_key = $sign.'-'.$increment;
		for($i=0; $i<count($history); $i++){
			if($i==0||empty($history)||count($history)<5){
				continue;
			}
			self::getCurrentLevel($i,$history_key);
		}
		return $this->levels;
	}
	
	public function getCurrentLevel($i=1,$history_key=null){
		if(!empty($history_key)&&!empty($this->history)){
			$history = $this->history[$history_key];
		} else {
			$history = self::getHistory(null,0,$increment);
		}
		$new = $history[$i];
		$last = $history[$i-1];
		$time = 0;
		$low = 1;
		$high = 2;
		$open = 3;
		$close = 4;
		$variability = config('services.algorithm.level_thickness');
		$consecutive = config('services.algorithm.level_consecutive');
		if($new[$close] > ($last[$close] + ($last[$close]*$variability))){
			$this->new_slope = 'bullish';
			$this->counter++;
		} else if($new[$close] < ($last[$close] - ($last[$close]*$variability))){
			$this->new_slope = 'bearish';
			$this->counter++;
		}
		if(!empty($this->last_slope)&&$this->new_slope!==$this->last_slope){
			if($this->counter > consecutive){
				$this->levels[$new[$time]] = [
					'level1'=>$new[$open],
					'slope'=>$this->last_slope
				];
				if($this->new_slope=='bullish'){
					$this->levels[$new[$time]]['level2'] = $last[$low];
				} else if($this->new_slope=='bearish'){
					$this->levels[$new[$time]]['level2'] = $last[$high];
				}
			}
			$this->counter = 1;
		}
		$last_level_time = array_key_last($this->levels);
		$last_level = $this->levels[$last_level_time];
		if(!empty($this->new_slope)&&$this->new_slope==$last_level['slope']){
			if($this->new_slope=='bullish'){
				$this->levels[$last_level_time]['level2'] = $new[$high];
			} else if($this->new_slope=='bearish'){
				$this->levels[$last_level_time]['level2'] = $new[$low];
			}
		}
		$this->last_slope = $this->new_slope;
		return $this->new_slope;
	}
	
	/**
		A head and shoulders pattern ocurrs when the following patterns are met:
		(1) (a) Get the highest point in the history
			(b) Split the history in half. We'll call these BH and AH (before high, afer high)
			(c) Going backwards on BH:
				(c.1) Proceed (backwards) until a candle closes higher than its predecessor (this is to get to the trough). Now slice the segment from trough to the end.
				(c.2) Get highest point of the new segment. Slice the segment between c.2 and c.1
				(c.3) Get lowest point of the new segment.
			(d) Going forwards on AH:
				Repeat step C going forwards on AH:
			(e) Get the current price
		(2) If c.2 and d.2 are within a degree of variability (ENV 1%) AND a is greater than both c.2 and d.2 by more than this degree of variability, this indicates a head and shoulders pattern could exist (it wouldn't exist if d.2 is the latest price).
		(3) If e is lower than d.3, head and shoulders is confirmed.
	**/	
	public function isHeadAndShoulders(){
		
	}

	public function getMACD($data,$start=null,$increment=null){
		$lower = min($data);
		$upper = max($data);
		$short = self::getEMA($lower,$start,$increment);
		$long = self::getEMA($upper,$start,$increment);
		if(empty($short)||empty($long)){
			return [];
		}
		$flag = false; // false indicates short is below long (sell)
		$this->signals = [];
		$prevS = null;
		$prevL = null;
		$margin = 0;
		$first_time = array_key_first($short);
		$first_price = $short[$first_time];
		$threshold = Config::get('services.algorithm.band_thickness')/2;
		$this->action = [];
		foreach($short as $t=>$s){
			$flag2 = $flag;
			$rtt = Macd::where('sign',$this->product)->where('time',$t)->where('bands',$lower.'-'.$upper)->where('increment',$increment)->first();
			if(!empty($rtt)){
				if($rtt->action!=='signal'){
					//$this->actions[$t] = ['point'=>$rtt->price,'action'=>$rtt->action];
					$this->action[$t] = $rtt->action;
				}
				$this->signals[$t] = $rtt->price;
				$prevS = $s;
				if(isset($long[$t])){
					//$flag = ($long[$t]<$s) ? true : false;
					if(($long[$t]+($long[$t]*$threshold))<$s){
						// If the price is higher than the higher edge of the thick long band
						$flag = true;
					} elseif(($long[$t]-($long[$t]*$threshold))>$s){
						// If the price is lower than the lower edge of the thick long band
						$flag = false;
					}
					$prevL = $long[$t];
				}
				continue;
			}
			if(isset($long[$t])){
				$this->band_margin[$t] = abs($s-$long[$t]);
				//$flag = ($long[$t]*<$s) ? true : false;
				if(($long[$t]+($long[$t]*$threshold))<$s){
					// If the price is higher than the higher edge of the thick long band
					$flag = true;
					$act = 'bull';
				} elseif(($long[$t]-($long[$t]*$threshold))>$s){
					// If the price is lower than the lower edge of the thick long band
					$flag = false;
					$act = 'bear';
				}
				$s_key = !empty(array_key_last($this->signals))? array_key_last($this->signals) : $first_time;
				if(!empty($prevL)&&$flag!==$flag2){
					$this->signals[$t] = $s;
					$rtt = new Macd();
					$rtt->sign = $this->product;
					$rtt->time = $t;
					$rtt->price = $s;
					$rtt->action = $act;
					//$this->actions[$t] = ['point'=>$s,'action'=>$act];
					$this->action[$t] = $act;
					$rtt->bands = $lower.'-'.$upper;
					$rtt->increment = $increment;
					$ps = isset($this->signals[$s_key])? $this->signals[$s_key] : $first_price;
					$rtt->signal_signal_margin = number_format(($s-$ps), 14);			// Margin from now to previous signal
					$rtt->save();
					$history = self::getHistory(null,0,$increment);
					$event = array_filter($history, function ($var) use($t) { return ($var[0] == $t); });
					$are = array_key_first($event);
					self::getAction($event[$are],$data,$increment);
				}
				$prevL = $long[$t];
			}
			$prevS = $s;
		}
		return $this->signals;
	}
	
	public function getActions($bands,$start,$increment){
		$history = self::getHistory(null,0,$increment);
		foreach($history as $event){
			self::getAction($event,$bands,$increment);
		}
		return $this->action;
	}
	
	/**
		getAction will only get the action for a given range and increment at a given time.
	**/
	public function getAction($event,array $bands,$increment=60){
		$lower = min($bands);
		$upper = max($bands);
		$t = $event[0];
		$lp = $event[1];
		$hp = $event[2];
		$ap = ($lp+$hp)/2;
		$macd = Macd::orderBy('time','DESC')->where('sign',$this->product)->where('bands',$lower.'-'.$upper)->where('increment',$increment)->first();
		$last = Action::orderBy('time','DESC')->where('sign',$this->product)->where('bands',$lower.'-'.$upper)->where('increment',$increment)->first();
		if(empty($macd)||!isset($this->bollinger_lower[$t])||!isset($this->bollinger_upper[$t])){
			return false;
		}
		// DAY TRADING: 1m & 5m charts
		if($increment<=300){
			// If low price crosses lower bollinger (oversold) AND MACD not bearish, buy
			if($macd->action!=='bear'&&$lp<$this->bollinger_lower[$t]){
				$act = 'buy';
				$action = Action::where('sign',$this->product)->where('time',$t)->where('bands',$lower.'-'.$upper)->where('increment',$increment)->first();
				if(!empty($action)&&!empty($last)&&$last->id==$action->id){
					$this->action[$t] = $action->action;
					return $this->action[$t];
				}
				if(empty($last)||$last->action!=$act){
					if(empty($action)){
						$action = new Action();
						$action->sign = $this->product;
						$action->time = $t;
						$action->increment = $increment;
						$action->bands = $lower.'-'.$upper;
						$action->price = $ap;
					}
					$this->action[$t] = $act;
					$action->action = $this->action[$t];
					$action->save();
					return $this->action[$t];
				}
			}
			// If high price crosses upper bollinger (overbought) AND MACD not bullish, sell
			if($macd->action!=='bull'&&$hp>$this->bollinger_upper[$t]){
				$act = 'sell';
				$action = Action::where('sign',$this->product)->where('time',$t)->where('bands',$lower.'-'.$upper)->where('increment',$increment)->first();
				if(!empty($action)&&!empty($last)&&$last->id==$action->id){
					$this->action[$t] = $action->action;
					return $this->action[$t];
				}
				if(empty($last)||$last->action!=$act){
					if(empty($action)){
						$action = new Action();
						$action->sign = $this->product;
						$action->time = $t;
						$action->increment = $increment;
						$action->bands = $lower.'-'.$upper;
						$action->price = $ap;
					}
					$this->action[$t] = $act;
					$action->action = $this->action[$t];
					$action->save();
					return $this->action[$t];
				}
			}
		} else {
		// LONG TRADING: 15m, 1h, 6h, 1d charts
		
			// MACD & Bollinger deviations
		}
		return false;
		/**
		
			$toonarrow = $this->deviation_band[$t] <= ($s*($this->fee*2));
			// If the current deviation is reducing its size (ie. stabilizing)
			$dev = array_reverse($this->deviation_band,true);
			$narrowed = $this->deviation_band[$t] < max(array_slice($dev,array_search($s_key,array_keys($dev)),floor(($t-$s_key)/$increment),true))*0.5;					
			// If the slope justifies an action:  2 consecutive non-action signals form an up or downslope matching the action type and the next signal margin projects the next signal is profitable
			$slope_justified = !array_key_exists($s_key,$this->action) && (($rtt->signal_signal_margin < 0 && $act == 'sell') || ($rtt->signal_signal_margin > 0 && $act == 'buy')) && ($rtt->signal_signal_margin*2 > ($s*($this->fee*2)));
			
			// If the net value is profitable
			$margin = number_format(($s-$p), 14);
			$profitable = abs($margin) >= abs($s*$this->fee);
		
		**/
	}

	public function getEMA($period=12,$start=null,$increment=null,$type='close'){
		$history = self::getHistory($start,0,$increment);
		switch($type){	// 0:time,1:low,2:high,3:open,4:close,5:volume
			case 'low':
				$type = 1;
				break;
			case 'high':
				$type = 2;
				break;
			case 'open':
				$type = 3;
				break;
			case 'close':
			default:
				$type = 4;
				break;
		}
		self::getSMA($period,$start,$increment,$type);
		if(false!==$history){
			$ema = 'ema_'.$period;
			$sma = 'sma_'.$period;
			$this->$ema = [];
			// history starts with the newest, so we're starting at the end, which is the oldest
			for($i=(count($history)-$period);$i>0;$i--){
				$emaobj = Ema::where('sign',$this->product)->where('time',$history[$i][0])->where('band',$period)->where('increment',$increment)->first();
				if(empty($emaobj)){
					$emaobj = new Ema();
				}
				$emaobj->sign = $this->product;
				$emaobj->time = $history[$i][0];
				$emaobj->increment = $increment;
				$emaobj->band = $period;
				if($i==(count($history)-$period)&&!empty($this->$sma[$history[$i-1][0]])){
					$emaobj->ema = $this->$ema[$history[$i-1][0]] = $this->$sma[$history[$i-1][0]];
				} else {
					$c = $history[$i-1][$type];				// Closing Price *today*
					$p = $this->$ema[$history[$i][0]];	// EMA *yesterday*
					$k = ($this->smoothing/($period+1));// Smoothing variable
					$calc = (($c*$k)+($p*(1-$k)));
					$emaobj->ema = $this->$ema[$history[$i-1][0]] = $calc;
				}
				$emaobj->save();
			}
			ksort($this->$ema);
			return $this->$ema;
		}
		return false;
	}

	/**		
		The increment isn't really used here, it's just passed to the history
	**/
	public function getSMA($period=5,$start=null,$increment=null,$type='close'){
		$history = self::getHistory($start,0,$increment);
		switch($type){	// 0:time,1:low,2:high,3:open,4:close,5:volume
			case 'low':
				$type = 1;
				break;
			case 'high':
				$type = 2;
				break;
			case 'open':
				$type = 3;
				break;
			case 'close':
			default:
				$type = 4;
				break;
		}
		if(false!==$history){
			$sma = 'sma_'.$period;
			$this->$sma = [];
			for($i=0;$i<count($history);$i++){
				$collection = [];
				if(isset($history[$i+1])&&(($history[$i][0])-($history[$i+1][0])) > $increment){
					$this->breaks[$history[$i][0]] = ['start'=>($history[$i][0])+1,'end'=>($history[$i+1][0])-1];
				}
				$sm = Sma::where('sign',$this->product)->where('time',$history[$i][0])->where('period',$period)->where('increment',$increment)->first();
				if(!empty($sm)){
					$this->$sma[$history[$i][0]] = $sm->price;
					$this->bollinger_upper[$history[$i][0]] = $sm->bollinger_upper;
					$this->bollinger_lower[$history[$i][0]] = $sm->bollinger_lower;
					$this->deviation_band[$history[$i][0]] = $sm->deviation;
					continue;
				}
				$sm = new Sma();
				$sm->sign = $this->product;
				$sm->period = $period;
				$sm->time = $history[$i][0];
				$sm->increment = $increment;
				for($y=$i;$y<($period+$i);$y++){
					if(!isset($history[$y])){
						break 2;
					}
					$collection[] = $history[$y][$type];
				}
				$sum = array_sum($collection);
				$count = count($collection);
				$sm->price = $this->$sma[$history[$i][0]] = $sum/$count;
				$standard_deviation = self::getStandardDeviation($collection);
				$sm->bollinger_upper = $this->bollinger_upper[$history[$i][0]] = ($sum/$count) + ($standard_deviation*2);
				$sm->bollinger_lower = $this->bollinger_lower[$history[$i][0]] = ($sum/$count) - ($standard_deviation*2);
				$sm->deviation = $this->deviation_band[$history[$i][0]] = $standard_deviation;
				$sm->save();
			}
			ksort($this->$sma);
			return $this->$sma;
		}
		return false;
	}
}

?>
