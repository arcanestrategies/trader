<?php
namespace App\Http\Controllers;

interface TradeInterface {

	public function __construct();	
	public static function getPrice($id);
	public function getProduct($id=null);
	public function getTicker();
	public function getTrades();
	public function getHistory($start=null,$end=0,&$increment=300,$timezone='America/New_York');
	public function getMACD($data,$start=null,$increment=null);
	public function getEMA($period=12,$start=null,$increment=null);
	public function getSMA($period=5,$start=null,$increment=null);
}

?>
