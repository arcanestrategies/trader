<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sign',
        'time',
        'price',
		'action',
		'increment',
		'bands',
		'order_id',
		'reason',
		'status'
    ];
}
