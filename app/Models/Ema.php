<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ema extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sign',
        'time',
		'increment',
        'ema',
		'band'
    ];
}