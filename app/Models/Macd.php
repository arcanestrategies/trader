<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Macd extends Model
{
	protected $table = 'macd';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sign',
        'time',
        'price',
		'action',
		'signal_action_margin',
		'signal_signal_margin',
		'action_action_margin'
    ];
}
