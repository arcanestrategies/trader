<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'sign',
		'base_currency',
		'quote_currency',
		'base_min_size',
		'base_max_size',
		'quote_increment',
		'base_increment',
		'display_name',
		'min_market_funds',
		'max_market_funds',
		'margin_enabled',
		'post_only',
		'limit_only',
		'cancel_only',
		'trading_disabled',
		'status',
		'status_message',
		'type'
    ];
}
