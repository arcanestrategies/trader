<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sma extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sign',
        'time',
		'increment',
        'period',
		'price',
		'billinger_upper',
		'billinger_lower',
		'deviation',
    ];
}