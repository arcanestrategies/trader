<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class ActionAlert extends Notification implements ShouldQueue
{
    use Queueable;
	
	public $message;
	//public $afterCommit = true;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message,$recipient=null)
    {
        $this->message = $message;
		$this->recipient = $recipient;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['nexmo'];
		return ['nexmo','slack'];
    }
	
	public function viaQueues()
	{
		return [
			//'nexmo' => 'nexmo-queue',
			//'mail' => 'mail-queue',
			//'slack' => 'slack-queue',
		];
	}

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }
	
	public function toNexmo($notifiable)
	{
		return (new NexmoMessage)
					->content($this->message);
	}

	public function toSlack($notifiable)
	{
		if(!empty($this->recipient)){
			return (new SlackMessage)
					->to($this->recipient)
					->content($this->message);
		}
		return (new SlackMessage)
					->content($this->message);
	}

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
