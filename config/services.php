<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],
	
	'nexmo' => [
		'sms_from' => env('NEXMO_SENDER'),
		'key' => env('NEXMO_KEY'),
		'key_name' => env('NEXMO_KEY_NAME'),
		'secret' => env('NEXMO_SECRET'),
		'signature' => env('NEXMO_SIGNATURE'),
		'hash' => env('NEXMO_HASH'),
		'sms_to' => env('NEXMO_RECIPIENT'),
	],
	
	'coinbase' => [
		'website' => env('COINBASE_WEBSITE'),
		'websocket' => env('COINBASE_WEBSOCKET'),
		'fix' => env('COINBASE_FIX'),
		'endpoint' => env('COINBASE_ENDPONT'),
		'nickname' => env('COINBASE_NICKNAME'),
		'passphrase' => env('COINBASE_PASSPHRASE'),
		'key' => env('COINBASE_KEY'),
		'secret' => env('COINBASE_SECRET'),
		'fee' => env('FEE'),
		'unit_limit' => env('UNIT_LIMIT',300)
	],
	
	'alpaca' => [
		'data_endpoint' => env('ALPACA_DATA_ENDPOINT'),
		'endpoint' => env('ALPACA_ENDPOINT'),
		'key' => env('ALPACA_KEY'),
		'alpaca_secret' => env('ALPACA_SECRET'),
		'fee' => env('FEE')
	],
	
	'slack' => [
		'webhook_coin' => env('SLACK_WEBHOOK_CRYPTO'),
		'webhook_btc' => env('SLACK_WEBHOOK_BTC')
	],
	
	'algorithm' => [
		'band_thickness' => env('BAND_THICKNESS',0),
		'kicker_band' => env('KICKER_BAND',2),
		'limit_variance' => env('LIMIT_VARIANCE',0.001),
		'level_consecutive' => env('LEVEL_CONSECUTIVE',3),
		'max_decimals' => env('MAX_DECIMALS',4),	// Only used for level indexes
		'bollinger_period' => env('BOLLINGER_PERIOD','long'),
		'deviation_period' => env('DEVIATION_PERIOD','short'),
		'amount' => [
						3600 => env('USD_EXCHANGE_VALUE_3600',750),
						900 => env('USD_EXCHANGE_VALUE_900',150),	
						300 => env('USD_EXCHANGE_VALUE_300',300),
						'default' => env('USD_EXCHANGE_VALUE',750),
					],
		'waits' => env('ENABLE_WAITS',false),
		'order_type' => env('ORDER_TYPE',false),
		'stagnant_threshold' => env('STAGNANT_THRESHOLD',0.015),
		'driving_price' => env('DRIVING_PRICE',0.01),
		'spike_pct' => env('SPIKE_PCT',false),
		'spike_x' => env('SPIKE_MULTIPLIER',false),
		'feeable_ema' => env('ENABLE_EMA_FEEABLE',false),
		'minute_triggers' => env('ENABLE_HR_CANDLE_MIN_TRIGGERS',false)
	],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
