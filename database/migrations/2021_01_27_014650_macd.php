<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Macd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('macd', function (Blueprint $table) {
			$table->id();
			$table->string('sign');
			$table->string('time');
			$table->string('price');
			$table->string('action');
			$table->string('increment')->nullable();
			$table->string('bands')->nullable();
			$table->string('signal_action_margin')->nullable();
			$table->string('signal_signal_margin')->nullable();
			$table->string('action_action_margin')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('macd');
    }
}
