<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreteaSmaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('smas', function (Blueprint $table) {
			$table->id();
			$table->string('sign');
			$table->string('increment');
			$table->string('period');
			$table->string('time');
			$table->string('price');
			$table->string('bollinger_upper')->nullable();
			$table->string('bollinger_lower')->nullable();
			$table->string('deviation')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smas');
    }
}
