<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
			$table->string('sign');
			$table->string('base_currency');
			$table->string('quote_currency')->default('USD');
			$table->string('base_min_size')->default('0.01');
			$table->string('base_max_size')->nullable();
			$table->string('quote_increment')->default('0.01');
			$table->string('base_increment')->default('0.01');
			$table->string('display_name');
			$table->string('min_market_funds')->nullable();
			$table->string('max_market_funds')->nullable();
			$table->string('margin_enabled')->nullable();
			$table->string('post_only')->nullable();
			$table->string('limit_only')->nullable();
			$table->string('cancel_only')->nullable();
			$table->string('trading_disabled')->nullable();
			$table->string('status')->nullable();
			$table->string('status_message')->nullable();
			$table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
