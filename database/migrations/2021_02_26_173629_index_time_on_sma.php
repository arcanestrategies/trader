<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndexTimeOnSma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('smas', function (Blueprint $table) {
			$table->index('time');
		});
        Schema::table('emas', function (Blueprint $table) {
			$table->index('time');
		});
        Schema::table('actions', function (Blueprint $table) {
			$table->index('time');
		});
        Schema::table('macd', function (Blueprint $table) {
			$table->index('time');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('smas', function (Blueprint $table) {
			$table->dropIndex('time');
		});
        Schema::table('emas', function (Blueprint $table) {
			$table->dropIndex('time');
		});
        Schema::table('actions', function (Blueprint $table) {
			$table->dropIndex('time');
		});
        Schema::table('macd', function (Blueprint $table) {
			$table->dropIndex('time');
		});
    }
}
