<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndexOtherGlobalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('smas', function (Blueprint $table) {
			$table->index('sign');
			$table->index('period');
			$table->index('increment');
			$table->index(['increment', 'period', 'sign']);
		});
        Schema::table('emas', function (Blueprint $table) {
			$table->index('sign');
			$table->index('band');
			$table->index('increment');
			$table->index(['increment', 'band', 'sign']);
		});
        Schema::table('actions', function (Blueprint $table) {
			$table->index('sign');
			$table->index('increment');
			$table->index('bands');
			$table->index(['increment', 'bands', 'sign']);
		});
        Schema::table('macd', function (Blueprint $table) {
			$table->index('sign');
			$table->index('increment');
			$table->index('bands');
			$table->index(['increment', 'bands', 'sign']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('smas', function (Blueprint $table) {
			$table->dropIndex('sign');
			$table->dropIndex('increment');
			$table->dropIndex('period');
			$table->dropIndex(['increment', 'period', 'sign']);
		});
        Schema::table('emas', function (Blueprint $table) {
			$table->dropIndex('sign');
			$table->dropIndex('band');
			$table->dropIndex('increment');
			$table->dropIndex(['increment', 'band', 'sign']);
		});
        Schema::table('actions', function (Blueprint $table) {
			$table->dropIndex('sign');
			$table->dropIndex('increment');
			$table->dropIndex('bands');
			$table->dropIndex(['increment', 'bands', 'sign']);
		});
        Schema::table('macd', function (Blueprint $table) {
			$table->dropIndex('sign');
			$table->dropIndex('increment');
			$table->dropIndex('bands');
			$table->dropIndex(['increment', 'bands', 'sign']);
		});
    }
}
