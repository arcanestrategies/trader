<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SizeAndFills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
			$table->decimal('size', 20)->default(0);
			$table->decimal('filled', 20)->default(0);
			$table->string('type')->default('limit');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('actions', function (Blueprint $table) {
			$table->dropColumn('size');
			$table->dropColumn('filled');
			$table->dropColumn('type');
		});
    }
}
