<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrackProjectionsAndPostOnly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
			$table->string('projected_price')->nullable();
			$table->string('limit_method')->default('any');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('actions', function (Blueprint $table) {
			$table->dropColumn('projected_price');
			$table->dropColumn('limit_method');
		});
    }
}
