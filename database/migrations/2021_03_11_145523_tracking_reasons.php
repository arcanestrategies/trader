<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrackingReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
			$table->integer('scenario')->default(0);
			$table->decimal('variance', 20)->default(0);
			$table->decimal('gradient_projection', 20)->default(0);
			$table->decimal('deviation', 20)->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('actions', function (Blueprint $table) {
			$table->dropColumn('scenario');
			$table->dropColumn('variance');
			$table->dropColumn('gradient_projection');
			$table->dropColumn('deviation');
		});
    }
}
