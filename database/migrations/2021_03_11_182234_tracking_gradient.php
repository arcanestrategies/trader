<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrackingGradient extends Migration
{
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
			$table->decimal('gradient', 20)->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('actions', function (Blueprint $table) {
			$table->dropColumn('gradient');
		});
    }
}
