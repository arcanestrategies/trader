<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ResolvingIncorrectDecimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('actions', function(Blueprint $t) {
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `size` `size` decimal(20,10) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `filled` `filled` decimal(20,10) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `variance` `variance` decimal(20,10) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `gradient_projection` `gradient_projection` decimal(20,10) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `deviation` `deviation` decimal(20,10) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `gradient` `gradient` decimal(20,10) default 0;'));
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('actions', function(Blueprint $t) {
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `size` `size` decimal(20,2) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `filled` `filled` decimal(20,2) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `variance` `variance` decimal(20,2) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `gradient_projection` `gradient_projection` decimal(20,2) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `deviation` `deviation` decimal(20,2) default 0;'));
			DB::statement(DB::raw('ALTER TABLE actions CHANGE COLUMN `gradient` `gradient` decimal(20,2) default 0;'));
		});
    }
}
