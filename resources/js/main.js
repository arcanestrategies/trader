$(document).ready(function(){
	
	const encryption_key = jQuery('meta[name="csrf-token"]').attr('content');
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': encryption_key
		}
	});
	
	setInterval(function() {
		var id = $('body').data('product');
		if(id.indexOf('-') > -1){
			var type = 'coin';
		} else {
			var type = 'stock';
		}
		var emas = $('body').data('emas');
		var increment = $('body').data('increment');
		formData = {product:id,type:type,emas:emas,increment:increment};
		getChart(formData);
		getOrders(formData);
	}, 120 * 1000);

	/*
	setInterval(function() {
		$('#favorites .row:visible').each(function(){
			var id = $(this).attr('id');
			var type = $(this).data('type');
			var emas = $('body').data('emas');
			var increment = $('body').data('increment');
			formData = {product:id,type:type,emas:emas,increment:increment};
			getmacd(formData);
			getaction(formData);
			// REFACTOR: getticker is exceeding API limits, so we may need to spread that apart, maybe run every 3 mins and pause every 1 second between actions
			getticker(formData);
		});
	}, 120 * 1000);
	*/
	
	var getOrders = function(formData){
		$.ajax({
		   type:'POST',
		   url:'/signal/orders',
		   data: formData,
		   dataType: 'html',
		   cache: false,
		   success:function(data) {
			  $('#orders').replaceWith(data);
			},
			error: function (data) {
				// Do nothing
			}
		});
	}
	
	
	var getChart = function(formData){
		$.ajax({
		   type:'POST',
		   url:'/signal/chart',
		   data: formData,
		   dataType: 'html',
		   cache: false,
		   success:function(data) {
			  $('#chartContainer').replaceWith(data);
			},
			error: function (data) {
				// Do nothing
			}
		});
	}
	
	var getmacd = function(formData){
		$.ajax({
		   type:'POST',
		   url:'/signal/macd',
		   data: formData,
		   dataType: 'json',
		   cache: false,
		   success:function(data) {
			  if(data===false){
				// Do nothing
			  } else {
				  if(data.action==='bear'){
					$('#favorites #'+formData.product+' [data-signal="macd"]').html('<i class="fa fa-arrow-down red"></i>');
				  } else if(data.action==='bull') {
					$('#favorites #'+formData.product+' [data-signal="macd"]').html('<i class="fa fa-arrow-up green"></i>'); 
				  } else {
					$('#favorites #'+formData.product+' [data-signal="macd"]').html('<i class="fa fa-wave-triangle yellow"></i>');
				  }
			  }
			},
			error: function (data) {
				// Do nothing
			}
		});
	}
	
	var getaction = function(formData){
		$.ajax({
		   type:'POST',
		   url:'/signal/action',
		   data: formData,
		   dataType: 'json',
		   cache: false,
		   success:function(data) {
			   // REFACTOR: If action has an order status, we should list that too.
			  if(data===false){
				// Do nothing
			  } else {
				  var d = new Date(0);
				  var timestamp = d.setUTCSeconds(data.time);
				  // REFACTOR: use a simpler string (EST instead of spelled out... no GMT-500)
				  var timestamp = d.toString();
				  if(data.action==='sell'){
					$('#favorites #'+formData.product+' [data-signal="action"]').html('<i class="fa fa-shopping-cart blue"></i> '+parseInt(data.price).toFixed(6)+' @ '+timestamp+'');
					const now = Math.round(Date.now() / 1000);
					if(now<=(data.time+data.interval)){
						$('#favorites #'+formData.product+' [data-signal="action"] i').addClass('pulse');
					}
				  } else if(data.action==='buy') {
					$('#favorites #'+formData.product+' [data-signal="action"]').html('<i class="fa fa-money-bill-alt green"></i> '+parseInt(data.price).toFixed(6)+' @ '+timestamp+''); 
				  } else {
					$('#favorites #'+formData.product+' [data-signal="action"]').html('<i class="fa fa-wave-triangle yellow"></i> '+parseInt(data.price).toFixed(6)+' @ '+timestamp+'');
				  }
			  }
			},
			error: function (data) {
				// Do nothing
			}
		});
	}
	
	// REFACTOR: There must be a client-side option out there for just getting the latest price.
	var getticker = function(formData){
		$.ajax({
		   type:'POST',
		   url:'/signal/ticker',
		   data: formData,
		   dataType: 'json',
		   cache: false,
		   success:function(data) {
			  if(data===false){
				// Do nothing
			  } else {
				$('#favorites #'+formData.product+' [data-signal="ticker"]').html(''+data+'');
			  }
			},
			error: function (data) {
				// Do nothing
			}
		});
	}
	
	var autosearch = function(){
		$("#search").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#favorites .row").filter(function() {
			  $(this).toggle($(this).attr('id').toLowerCase().indexOf(value) > -1);
			});
		})
	};

	var search = function(){
		$('[data-ajaxable="post"]').click(function(e){
			if(typeof $(this).data('action') !== "undefined"){
				var ajaxurl = $(this).data('action');
				var resultcontainer = '#status';
				if(typeof $(this).data('results')!== "undefined"){
					var resultcontainer = $(this).data('results');
				}
				if(typeof $("form")!=="undefined"){
					var formContainer = $(this).parentsUntil("form");
				} else if ($(".input-group")!=="undefined"){
					var formContainer = $(this).parentsUntil(".input-group");
				}
				var formData = formContainer.find(':input').serialize();
				$.ajax({
				   type:'POST',
				   url:ajaxurl,
				   data: formData,
				   dataType: 'json',
				   cache: false,
				   success:function(data) {
					  if(data===false){
						var result_row = '<p class="alert alert-error">Sorry, no results</p>';
						$(resultcontainer).html(result_row);
					  } else {
						var result_row = '<div id="'+data.sign+'" class="row col-md-12 mb-2">';
						/** REFACOTR: dynamic options **/
						result_row += '<div class="col-md-3"><a href="/?emas=12,26&increment=300&product='+data.sign+'" class="btn btn_primary" name="'+data.sign+'">'+data.sign+'</a></div>';
						result_row += '<div data-signal="macd" class="col-md-1"><i class="fa fa-spinner"></i></div>';
						result_row += '<div data-signal="action" class="col-md-5"><i class="fa fa-spinner"></i></div>';
						result_row += '<div data-signal="ticker" class="col-md-2"><i class="fa fa-spinner"></i></div>';
						result_row += '<div class="col-md-1"><span data-ajaxable="post" data-action="track/add" class="green"><i class="fa fa-check"></i></span></div>';
						$(resultcontainer).html(result_row);
					  }
					},
					error: function (data) {
						var result_row = '<p class="alert alert-error">Sorry, no results</p>';
						$(resultcontainer).html(result_row);
					}
				});
			}
		});
	};
	
	search();
	autosearch();

});