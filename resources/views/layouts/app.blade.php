<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}}</title>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script src="{{asset('js/app.js')}}" defer></script>
	<link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>
<!-- REFACTOR: Make these dynamic -->
<body data-emas="{{app('request')->input('emas') ?? '12,26' }}" data-increment="{{app('request')->input('increment') ?? '300' }}" data-product="{{app('request')->input('product') ?? 'BTC-USD' }}" class="dark">

@include('partials.header')

@yield('content')

@include('partials.footer')

</body>
</html>