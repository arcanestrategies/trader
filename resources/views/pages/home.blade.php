@extends('layouts.app')

@section('title')
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
		@include('partials.status')
	</div>
</div>
<div class="row col-md-12">
	<div class="col-md-4">
		@include('partials.sidebar')
	</div>
	<div class="col-md-8">
		@include('partials.chart')
		@include('partials.history')
	</div>
</div>
@endsection