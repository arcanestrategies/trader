@extends('layouts.app')

@section('title')
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
		@include('partials.status')
	</div>
</div>
<div class="row col-md-12">
	<div class="col-md-4">
		<h2>Product</h2>
		<select name="products" id="products">
		  @foreach($products as $sign)
			<option value="{{ $sign }}">{{ $sign }}</option>
		  @endforeach
		</select>
		<h2>Reason</h2>
		<select name="reasons" id="reasons" onchange="location = this.value;">
		  @foreach($reasons as $reason)
			<option value="/reports/?reason={{ $reason }}">{{ $reason }}</option>
		  @endforeach
		</select>
		@foreach($options as $key=>$opts)
		<h2>{{ $key }}</h2>
			<select name="reasons" id="reasons">
			  @foreach($opts as $option)
				<option value="{{ $option }}">{{ $option }}</option>
			  @endforeach
			</select>
		@endforeach
	</div>
	<div class="col-md-8">
		@include('partials.reports')
	</div>
</div>
@endsection