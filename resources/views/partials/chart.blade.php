	<div id="chartContainer" style="height: 370px; width: 100%;"></div>
	<script>
	var d = new Date(0);
	var chart = new CanvasJS.Chart("chartContainer",
		{
		theme: "dark2",
		zoomEnabled: true,
		legend:{
			//legend properties
		},
		axisY:{
			includeZero: false,
			@if(!empty($levels) && true===false)
				stripLines:[
					@php
						foreach($levels as $price=>$r){
							if(is_array($r)){
								echo '{startValue: '.$r["level1"].',endValue: '.$r["level2"].',color:"#3490dc"},';
							}
						}
					@endphp
				],
			@endif
		},
		@if(!empty($breaks))
		 axisX: {
			 scaleBreaks: {
				 customBreaks: [
					@php
						foreach($breaks as $b){
							if(!empty($b)){
								echo '{startValue: '.$b['start'].', endValue: '.$b['end'].', type: "straight", lineThickness:1, spacing: 0},';
							}
						}
					@endphp
				 ]
			 },
		},
		@endif
		data: [
		@php
			if(!empty($smas_output)){
				foreach($smas_output as $sm=>$output){
					echo '{ type: "line", showInLegend: true, legendText: "SMA '.$sm.'", dataPoints: [ ';
					if(!empty($output)){
						foreach($output as $t=>$s){
							echo '{x: '.$t.',y:'.$s.'},';
						}
					}
				echo ']},';
				}
			}
			if(!empty($history)){
				echo '{ type: "candlestick", dataPoints: [ ';
						foreach($history as $h){
							if(is_array($h)){
								echo '{x: '.$h[0].',y:['.$h[3].','.$h[2].','.$h[1].','.$h[4].']},';
							}else{
								echo '{x: '.$h->t.',y:['.$h->o.','.$h->h.','.$h->l.','.$h->c.']},';
							}
						}
				echo ']},';
			}
			if(!empty($levels)){
				echo '{ type: "error", whiskerColor: "#FFA500", stemColor: "#FFA500", dataPoints: [ ';
						foreach($levels as $price=>$r){
							if(is_array($r)&&$r['type']=='continued'){
								echo '{x: '.$r["time"].',y: ['.$r["level1"].','.$r["level2"].']},';
							}
						}
				echo ']},';
				echo '{ type: "error", whiskerColor: "#0000ff", stemColor: "#0000ff", dataPoints: [ ';
						foreach($levels as $price=>$r){
							if(is_array($r)&&($r['type']!='continued')){
								echo '{x: '.$r["time"].',y: ['.$r["level1"].','.$r["level2"].']},';
							}
						}
				echo ']},';
			}
			if(!empty($emas_output)){
				$k = 0;
				foreach($emas_output as $em=>$output){
					echo '{ type: "line", showInLegend: true, legendText: "EMA '.$em.'", dataPoints: [ ';
					if(!empty($output)){
						foreach($output as $t=>$s){
							echo '{x: '.$t.',y:'.$s;
							if(!empty($actions)&&isset($actions[$t])&&$k===0){
								echo ', indexLabel: "'.((isset($actions[$t]['status'])&&$actions[$t]['status']=='wait') ? 'wait' : $actions[$t]['action']).'", markerColor: "'.(($actions[$t]['action']==='buy')? 'green' : (($actions[$t]['action']==='sell')? 'red' : 'yellow')).'", markerType: "'.(($actions[$t]['action']==='buy'||$actions[$t]['action']==='sell')? 'triangle' : 'circle').'", markerSize: 10';
							}
							echo '},';
						}
					}
					echo ']},';
					$k++;
				}
			}
			if(!empty($bollinger)){
				foreach($bollinger as $em=>$output){
					echo '{ type: "line", showInLegend: true, legendText: "Bollinger '.$em.'", dataPoints: [ ';
					if(!empty($output)){
						foreach($output as $t=>$s){
							echo '{x: '.$t.',y:'.$s.'},';
						}
					}
					echo ']},';
				}
			}
			@endphp
		  ]
		});

	 chart.render();
	 
	 // REFACTOR: This should be moved to a different file.
	</script>