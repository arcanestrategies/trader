<div id="orders">
	<h2>Order History</h2>
	<p>Oldest (top) to Newest (bottom)</p>
	@if(!empty($orders))
		<table>
		<thead>
			<tr>
				<th>
					Product
				</th>
				<th>
					Action
				</th>
				<th>
					Bands
				</th>				
				<th>
					Increment
				</th>
				<th>
					Time
				</th>
				<th>
					Price
				</th>
				<th>
					Reason
				</th>
				<th>
					Status
				</th>
				<th>
					Action Gross Margin
				</th>
			</tr>
		</thead>
		@php
			$am = $gtp = $gsp = $ntp = $rnp = $rgp = $nsp = $gpp = $npp = 0;
			$i = 0;
			$last_price = 0;
		@endphp
		@foreach($orders as $order)
			@php
				if($order->action=='buy'){
					$order->price = ($order->price)*-1;
				}
				if($order->status=='done'){
					if($i==0){
						$first_action = $order->action;
						if(empty($first_price)){
							$first_price = $order->price;
						}
						if($first_action == 'sell'){
							$rgp = abs($order->price)-abs($first_price);
						} else {
							$rgp = abs($first_price)-abs($order->price);
						}
					} else {
						$rgp = ($order->price)+$last_price;
					}
					$fee = abs($order->price*((config('services.coinbase.fee')/2)));
					$rnp = $rgp-$fee;
					if($order->action=='sell'){
						$gsp = $gsp+$rgp;
						$nsp = $nsp+$rnp;
					} else if($order->action=='buy'){
						$gpp = $gpp+$rgp;
						$npp = $npp+$rnp;
					}
					$gtp = $gtp+$rgp;
					$ntp = $ntp+$rnp;
					$i++;
					$last_price = $order->price;
				}
			@endphp
			<tr>
				<td>{{ strtoupper($order->sign) }}</td>
				<td>{{ ucfirst($order->action) }}</td>
				<td>{{ $order->bands }}</td>
				<td>{{ $order->increment }}</td>
				<td>{{ $order->updated_at }}</td>
				<td>{{ number_format(abs($order->price),6) }}</td>
				<td>{{ $order->reason }}</td>
				<td>{{ !empty($order->status)? ucfirst($order->status) : 'No Action' }}</td>
				@if($order->status=="done")
					@if(!empty($order->price))
						<td class="{{ ($rgp > 0) ? (($order->action=='buy')? 'green' : 'blue') : (($order->action=='buy')? 'red' : 'orange') }}">{{ number_format($rgp,6) }} / {{ number_format(($rgp/abs($order->price)*100),2) }}%</td><!-- Margin between this and previous -->
					@else
						<td>0</td>
					@endif
				@else
				<td></td>
				@endif
			</tr>
		@endforeach
		</table>
		<div class="mt-4 mb-4"></div>
	@endif
	<div class="mt-4 mb-4"></div>
	<div>
		<h3>Key:</h3>
		<ul>
			<li><strong>Bull</strong> <span>This yellow dot indicates a positive MACD crossover.  This occurs when the short exponential moving average (ema) crosses upward over the long ema/band.  This is known as a bullish crossover, which is a positive indicator.</li>
			<li><strong>Bear</strong> <span>This yellow dot indicates a negative MACD crossover.  This occurs when the short exponential moving average (ema) crosses downward over the long ema/band.  This is known as a bearish crossover, which is a negative indicator.</li>
			<li><strong>Buy</strong> <span>This green icon indicates a spot in time where a buy action occurred.  Even though the icon exists on the trend line, it is actually fired on the candle itself, so be sure to look above or below the icon for the candle from the same time, for a more accurate price.</li>
			<li><strong>Sell</strong> <span>This red icon indicates a spot in time where a sell action occurred.  Even though the icon exists on the trend line, it is actually fired on the candle itself, so be sure to look above or below the icon for the candle from the same time, for a more accurate price.</li>
			<li><strong>Orange Lines</strong> <span>You may notice little orange vertical lines with whiskers at the top and bottom of a wave.  This is a calculated "level".  A level occurs when a pattern indicates a change in behavior.  These are used to build support and resistance levels for pattern identification.  These are hugely important.</li>
			<li><strong>Blue Bands</strong> <span>The blue bands in the background are horizontal indicators of levels.  These move forward and backward but in a true pattern a level wouldn't exist until it is created, so do not use these for anything other than forward projections.</li>
			<li><strong>Gross Action Margin</strong> <span>Total sum of margin between every action (per share) (we want this +)</span></li>
			<li><strong>Gross Sales Margin</strong> <span>The total sum of margin occurring from sales only (per share) (we want this +)</span></li>
			<li><strong>Net Total Margin</strong> <span>This is the total sum of margin occurring from every action (per share) minus fees (we want this +)</span></li>
			<li><strong>Net Sales Margin</strong> <span>This is the total sum of margin occurring from sales only (per share) minus fees (we want this +)</span></li>
			<li><strong>Gross Chart Margin</strong> <span>This is what we're compared against if we took no action at all (per share)</span></li>
			<li><strong>Total Net Margin</strong> <span>This is the net total margin minus the gross chart margin.  It indicates how well we are doing in comparison to the chart.  This is what matters most! (we want this +)</span></li>
		</ul>
	</div>
</div>