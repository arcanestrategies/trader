<div id="sidebar">
	<div class="row col-md-12">
		<div id="search-container" class="input-group">
		  <div class="form-outline mb-2 w-100 row">
			<div class="col-md-8">
				<input type="search" placeholder="Search Sign" name="search" id="search" class="form-control w-100" />
			</div>
			<div class="col-md-4">
				<button class="btn btn_primary" data-ajaxable="post" data-results="#signs" data-action="/search/sign">Search</button>
			</div>
		  </div>
		</div>
	</div>
	<div id="signs">
		<!-- signs are replaced by search results -->
	</div>
	@if(!empty($favorites))
	<div id="favorites">
		@foreach($favorites as $type=>$list)
			@if(!empty($list))
				@foreach($list as $option)
				<div id="{{$option->sign}}" data-type="{{$type}}" class="row col-md-12 mb-2">
					<div class="col-md-3">
						<!-- REFACTOR : Make this dynamic -->
						<a href="/?emas=12,26&increment=300&product={{$option->sign}}" class="btn btn_primary" name="{{$option->sign}}">{{$option->sign}}</a>
					</div>
					<div data-signal="macd" class="col-md-1"><i class="fa fa-spinner"></i></div>
					<div data-signal="action" class="col-md-5"><i class="fa fa-spinner"></i></div>
					<div data-signal="ticker" class="col-md-2"><i class="fa fa-spinner"></i></div>
					<div class="col-md-1">
						<span class="red" data-ajaxable="delete" data-action="track/delete"><i class="fa fa-times"></i></span>
					</div>
				</div>
				@endforeach
			@endif
		@endforeach
	</div>
	@endif
</div>