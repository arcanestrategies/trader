<div id="status">
@if(!empty($status))
<div class="w-100"><p class="alert alert-{{!empty($type)? $type : 'warning'}}">
	{{$status}}
</p></div>
@endif
</div>