<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('',[HomeController::class, 'index'])->name('profile');
Route::get('reports',[ReportController::class, 'getReports'])->name('reports');
Route::group(['prefix' => 'search'], function(){
		Route::post('sign',[HomeController::class, 'searchSigns'])->name('search');
});
Route::group(['prefix' => 'signal'], function(){
		Route::post('macd',[HomeController::class, 'getMacd'])->name('signalMacd');
		Route::post('action',[HomeController::class, 'getAction'])->name('signalAction');
		Route::post('ticker',[HomeController::class, 'getTicker'])->name('signalTicker');
		Route::post('chart',[HomeController::class, 'showChart'])->name('signalChart');
		Route::post('orders',[HomeController::class, 'showOrders'])->name('signalOrders');
});